<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

use EveAnon\Service\Igb;

App::before(
    function ($request) {
        //
    }
);


App::after(
    function ($request, $response) {
        //
    }
);

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

/**
 * Legacy - was used to limit access to app to friends only.
 */
Route::filter(
    'eve-auth',
    function () {
        /** @var Igb $igb */
        $igb = App::make('Igb');
        if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1' && !in_array($igb->header('EVE_CHARNAME'), Config::get('evefriends'))) {
            return View::make('errors.forbidden');
        }
    }
);

Route::filter(
    'eve-admin',
    function () {
        /** @var Igb $igb */
        $igb = App::make('Igb');
        if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1' && !in_array($igb->header('EVE_CHARNAME'), array('Innately Awesome', 'sagant'))) {
            return View::make('errors.forbidden');
        }
    }
);

Route::filter(
    'eve-igb', function () {
        /** @var Igb $igb */
        $igb = App::make('Igb');
        if (!$igb->header('EVE_TRUSTED')) {
            return View::make('errors.message', array('message' => 'Браузер-то не тот.'));
        } elseif ($igb->header('EVE_TRUSTED') != 'Yes') {
            return View::make('errors.igb.trust');
        }
    }
);

Route::filter(
    'eve-bans', function () {
        $igb = App::make('Igb');
        if ($igb->header('EVE_CHARNAME') != 'Innately Awesome' && in_array($igb->header('EVE_CORPID'), array('98234945'))) {
            return View::make('signatures.banned');
        }
    }
);

Route::filter(
    'auth', function () {
        if (!Sentry::check()) {
            return Redirect::to('login');
        }
    }
);

Route::filter(
    'web.editors', function () {
        if (!Sentry::check() || !Sentry::getUser()->hasAccess('web.edit')) {
            throw new \EveAnon\Exceptions\AccessForbiddenException('Доступ к этой странице есть только у редакторов.');
        }
    }
);

Route::filter(
    'web.admins', function () {
        if (!Sentry::check() || !Sentry::getUser()->hasAccess('web.admin')) {
            throw new \EveAnon\Exceptions\AccessForbiddenException('Доступ к этой странице есть только у администраторов.');
        }
    }
);


Route::filter(
    'auth.basic', function () {
        return Auth::basic();
    }
);

Route::filter(
    'force.ssl', function () {
        if (App::environment() != 'development' && !Request::secure()) {
            return Redirect::secure(Request::getRequestUri());
        }
    }
);

/*App::before(
    function ($request) {
        if (!Request::secure()) {
            return Redirect::secure(Request::path());
        }
    }
);*/

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter(
    'guest', function () {
        if (Auth::check()) {
            return Redirect::to('/');
        }
    }
);

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter(
    'csrf', function () {
        if (Session::token() != Input::get('_token')) {
            throw new Illuminate\Session\TokenMismatchException;
        }
    }
);
