@extends('signatures.layout')

@section('content')
<h2>All known signatures</h2>
@if(count($signatures) > 0)
        @include('signatures.partial.index', array('signatures' => $signatures))
@else
    <p>No signatures found!</p>
@endif

@stop
