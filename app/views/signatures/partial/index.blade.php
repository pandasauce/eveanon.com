<table class="table table-hover table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Scan Group</th>
        <th>Group</th>
        <th>Type</th>
        <th>Signal</th>
        <th>System</th>
    </tr>
    </thead>
    <tbody>
@foreach($signatures as $signature)
        <tr>
            <td>{{ $signature->id }}</td>
            <td>{{ $signature->scan_group }}</td>
            <td>{{ $signature->group }}</td>
            <td>{{ $signature->type }}</td>
            <td>{{ $signature->signal }}</td>
            <td>{{ $signature->system->id }}</td>
        </tr>
@endforeach
    </tbody>
</table>
