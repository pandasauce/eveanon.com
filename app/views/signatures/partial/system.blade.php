<h3>
    {{$system->id}}
    <br/>
    <small>{{count($signatures)}} results
        @if(App::make('Igb')->isAdminMode())
        <a href="/signatures/destroy/{{$system->id}}">Clear</a>
        @endif
    </small>
</h3>
<style>
    td, td {
        width: 25%;
    }
</style>
<table class="table table-hover table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Scan Group</th>
        <th>Group</th>
        <th>Type</th>
        <th>Signal</th>
    </tr>
    </thead>
    <tbody>
@foreach($signatures as $signature)
        <tr>
            <td>{{$signature->id}}</td>
            <td>{{$signature->scan_group}}</td>
            <td>{{$signature->group}}</td>
            <td>{{$signature->type}}</td>
            <td>{{$signature->signal}}</td>
        </tr>
@endforeach
    </tbody>
</table>
