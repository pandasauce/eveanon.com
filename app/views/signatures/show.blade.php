@extends('signatures.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <br/>

        <form role="form" class="form-inline" action="/signatures" method="POST">
            <textarea name="probescan" id="probescan" class="form-control" rows="1"></textarea>
            <button type="submit" class="btn btn-primary">Send</button>
        </form>
        @include('signatures.partial.system', array('system' => $system, 'signatures' => $signatures))
    </div>
</div>
<script>
@if (App::environment() == 'production')
    function derp() {
        window.location.reload();
    }
    setTimeout("derp()", 10000);
@endif
</script>
@stop
