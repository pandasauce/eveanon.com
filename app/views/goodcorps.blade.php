@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>Как найти PVE корпорацию</h2>

            <p>
                С PVP корпорациями в EVE Online вроде всё понятно - есть всевозможные сортировки на
                <a href="http://zkillboard.com/">киллборде</a> и остаётся только разузнать про компенсации. С поиском PVE
                корпораций всё сложнее. Главные вопросы, которые задаёт себе капсулир, который ищет PVE корпорацию: </p>
            <ul>
                <li>Какие в корпорации налоги?</li>
                <li>Какая непись в регионе?</li>
                <li>Какие системы доступны?</li>
            </ul>
            <p>
                И уже на первом вопросе начинаются проблемы. Приходится ручками пробивать каждую корпорацию в альянсе и
                смотреть налоговую ставку. В особо тяжёлых случаях в инфо окошке налог стоит нулевой, а на самом деле
                платить нужно от 100 до 300 миллионов ISK в месяц.
                Задача этой страницы - <b>облегчить поиск PVE-корпорации.</b>
                Сразу же отсеяны корпорации с налогом выше, чем в NPC-корпорации, и корпорации
                ботоводов с менее десяти мемберов - туда набор всё равно никто не ведёт. </p>

            <p>
                Фича здесь - <b>сортировка по официальным™ налогам</b> корпораций в ситизен-альянсах.
                Все отзывы можно отправить игровой почтой на персонажа
                <a target="_blank" href="https://evewho.com/pilot/PANDApwnz 1-800-TEARS">
                    <span class="igbinfo"></span>PANDApwnz 1-800-TEARS</a>.
                К слову, наличие иконки
                <span class="igbinfo"></span> означает,
                что клик по ссылке откроет окно с информацией о предмете через дотлан или eve-who. </p>
            {{ $corpsTable }}
        </div>
    </div>
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="/js/jquery.tablesorter.min.js"></script>
    <script>
        $('.apiname').on('click', function (event) {
            var corpid = $(this).attr('id');
            CCPEVE.showInfo(2, corpid);
        });
        $('.apiallianceheading').on('click', function (event) {
            var allyid = $(this).attr('id');
            CCPEVE.showInfo(16159, allyid);
        });
        function showAlliance(allyid) {
            $('.apiallianceblock').each(function () {
                $(this).hide()
            });
            $('#' + allyid).show();
        }
        $(document).ready(function () {
            $('.tablesorter').each(function (index) {
                $(this).tablesorter()
            });
            var i = 0;
            $('.apiallianceblock').each(function () {
                $(this).hide()
            });
            $('.apiallianceblock').first().show();
        });
    </script>
@stop
