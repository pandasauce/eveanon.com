@extends('posts.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1 class="text-center post-long-title">{{ $post['long_title'] }}</h1>
        <h5 class="text-center post-readable-date"><span data-toggle="tooltip" data-placement="top" title="Дата поста">{{ $post['readable_date'] }}</span> / <span data-toggle="tooltip" data-placement="bottom" title="Автор поста">{{ $post['author'] }}</span></h5>
        {{ $post['full'] }}
        @if(App::environment() == 'production' && $post['bool_comments'])
            @include('partial.disqus')
        @endif
    </div>
</div>
@stop
