@extends('posts.layout')

@section('head')
@parent
<script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/js/tinymce/jquery.tinymce.min.js"></script>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1>EVE Anon
            <small>редактирование</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <form action="{{ action('EveAnon\Controller\PostController@postEdit', [$post->id]) }}" method="POST">
            <div class="form-group">
                <label><span data-toggle="tooltip" data-placement="right" title="для фида">Короткий заголовок</span></label>
                <input type="text" name="shortTitle" id="shortTitle" class="form-control"
                       value="{{ htmlspecialchars($post->shortTitle) }}">
            </div>
            <div class="form-group">
                <label><span data-toggle="tooltip" data-placement="right" title="для страницы поста">Длинный заголовок</span></label>
                <input type="text" name="longTitle" id="longTitle" class="form-control"
                       value="{{ htmlspecialchars($post->longTitle) }}">
            </div>
            <div class="form-group">
                <label><span data-toggle="tooltip" data-placement="right" title="для SEO и RSS">Описание</span></label>
                <textarea name="postDescription" id="postDescription" rows="2" class="form-control">{{ $post->postDescription }}</textarea>
            </div>
            <div class="form-group">
                <label><span data-toggle="tooltip" data-placement="right" title="для URL страницы поста">Slug поста</span></label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{ htmlspecialchars($post->slug) }}">
            </div>
            <div class="form-group">
                <label><span data-toggle="tooltip" data-placement="right" title="да, можно постить задним числом">Дата</span></label>
                <input type="text" name="created_at" id="created_at" class="form-control"
                       value="{{ htmlspecialchars($post->created_at) }}">
            </div>
            <div class="form-group">
                <label><span data-toggle="tooltip" data-placement="right" title="Правой кнопкой - Source Code &lt;!-- more --&gt; чтобы прятать под кат">HTML поста</span></label>
                <textarea name="rawHtml" id="rawHtml" rows="10" class="form-control">{{ $post->rawHtml }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
                <div class="checkbox">
                    <label><input type="checkbox" name="boolPublic" id="boolPublic" value="1"
                        {{ $post->boolPublic ? 'checked' : '' }}>Отображать в ленте</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="boolComments" id="boolComments" value="1"
                        {{ $post->boolComments ? 'checked' : '' }}>Включить комментарии</label>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-5">
        <p><a href="/posts/edit" class="btn btn-primary">Запостить новую чушь</a></p>

        <h2>Предыдущие посты</h2>
        <dl>
            @foreach ($feed['posts'] as $feedPost)
            <dt class="edit-paged-old">
                <a href="{{ $feedPost['link'] }}" class="post-short-title">{{ $feedPost['short_title']
                    }}</a>
                <br>
                <small class="post-readable-date">{{ $feedPost['readable_date'] }} - {{ $feedPost['author']
                    }}
                </small>
            </dt>
            <dd>{{ $feedPost['page_description'] }}</dd>
            @endforeach
        </dl>
        {{ $feed['links'] }}
    </div>
</div>
<br>
@stop

@section('scripts')
@parent
<script>
    function convertToSlug(Text) {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-')
            ;
    }
    var a = {
        "Ё": "YO",
        "Й": "I",
        "Ц": "TS",
        "У": "U",
        "К": "K",
        "Е": "E",
        "Н": "N",
        "Г": "G",
        "Ш": "SH",
        "Щ": "SCH",
        "З": "Z",
        "Х": "H",
        "Ъ": "'",
        "ё": "yo",
        "й": "i",
        "ц": "ts",
        "у": "u",
        "к": "k",
        "е": "e",
        "н": "n",
        "г": "g",
        "ш": "sh",
        "щ": "sch",
        "з": "z",
        "х": "h",
        "ъ": "'",
        "Ф": "F",
        "Ы": "I",
        "В": "V",
        "А": "a",
        "П": "P",
        "Р": "R",
        "О": "O",
        "Л": "L",
        "Д": "D",
        "Ж": "ZH",
        "Э": "E",
        "ф": "f",
        "ы": "i",
        "в": "v",
        "а": "a",
        "п": "p",
        "р": "r",
        "о": "o",
        "л": "l",
        "д": "d",
        "ж": "zh",
        "э": "e",
        "Я": "Ya",
        "Ч": "CH",
        "С": "S",
        "М": "M",
        "И": "I",
        "Т": "T",
        "Ь": "'",
        "Б": "B",
        "Ю": "YU",
        "я": "ya",
        "ч": "ch",
        "с": "s",
        "м": "m",
        "и": "i",
        "т": "t",
        "ь": "'",
        "б": "b",
        "ю": "yu"
    };

    function transliterate(word) {
        return word.split('').map(function (char) {
            return a[char] || char;
        }).join("");
    }
    $(document).ready(function(){
        $('textarea#rawHtml').tinymce({
            menubar: false,
            plugins: "link autolink preview media image contextmenu table code",
            contextmenu: "link image | code"
        });
        $('#longTitle').on('input', function () {
            var longTitle = $(this).val();
            if (longTitle.length > 128) {
                $(this).val(longTitle.slice(0, 128));
                return;
            }
        });
        $('#postDescription').on('input', function () {
            var postDesc = $(this).val();
            if (postDesc.length > 140) {
                $(this).val(postDesc.slice(0, 140));
                return;
            }
        });
        $('#slug').on('input', function () {
            var slug = $(this).val();
            if (slug.length > 64) {
                $(this).val(slug.slice(0, 64));
                return;
            }
        });
        $('#created_at').on('input', function () {
            var createdAt = $(this).val();
            var reDate = /^201(3|4)\-(0|1)[0-9]\-(0|1|2|3)[0-9]\ (0|1|2)[0-9]\:(0|1|2|3|4|5|6)[0-9]\:(0|1|2|3|4|5|6)[0-9]$/;
            if (reDate.test(createdAt) == true) {
                $(this).parent().removeClass('has-error');
                $(this).parent().addClass('has-success');
            }
            else {
                $(this).parent().removeClass('has-success');
                $(this).parent().addClass('has-error');
            }
        });
        $('#shortTitle').on('input', function () {
            var shortTitle = $(this).val();
            if (shortTitle.length > 64) {
                $(this).val(shortTitle.slice(0, 64));
                return;
            }
            if (($('#longTitle').val() == "Новый пост") || ($('#longTitle').val() == shortTitle.slice(0, shortTitle.length - 1))) $('#longTitle').val(shortTitle);
            shortTitle = transliterate(shortTitle);
            $('#slug').val(convertToSlug(shortTitle));
        });
    });
</script>
@stop
