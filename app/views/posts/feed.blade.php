@extends('posts.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1 class="text-center"><img src="/img/ico/eveanon.png" alt="EVE Anon"> EVE Anon
            <small>без регистрации и смс</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12" id="post-feed">
        @foreach($feed['posts'] as $post)
        <h2 class="post-short-title"><a href="{{ $post['link'] }}">{{ $post['short_title'] }}</a></h2>
        <h6 class="post-readable-date clearfix"><span class="pull-left" data-toggle="tooltip" data-placement="right" title="Дата поста">{{ $post['readable_date'] }}</span> <span class="pull-right" data-toggle="tooltip" data-placement="left" title="Автор поста">{{ $post['author'] }}</span></h6>

        <div class="post-wrap">
            {{ $post['excerpt'] }}
        </div>
        @endforeach
        @if(empty($feed['posts']))
            <p>Ничего не нашлось.</p>
        @endif
        {{ $feed['links'] }}
    </div>
</div>
@stop
