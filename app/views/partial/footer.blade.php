<footer id="footer">
    <div class="container">
        <h4 class="pull-left"><a href="/copyrights">&copy;</a> EVE Anon v3.6
            <small>| Почта: <a href="" title="включи javascript чтобы увидеть" id="fdbk">batya</a> | Ingame: <a
                    target="_blank" href="https://evewho.com/pilot/PANDApwnz 1-800-TEARS">PANDApwnz 1-800-TEARS <span
                        class="igbinfo"></span></a></small>
        </h4>
        <div class="social pull-right">
            <a href="#vk"
               onclick="window.open('http://vk.com/share.php?url={{ Request::url() }}', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=200, top=200, width=554, height=421, toolbar=0, status=0');return false"><img
                    src="/img/soc/vk.png" alt="VKontakte"></a>
            <a href="#twitter"
               onclick="window.open('https://twitter.com/share?url='+encodeURIComponent('{{ Request::url() }}'), '_blank', 'scrollbars=0, resizable=1, menubar=0, left=200, top=200, width=554, height=421, toolbar=0, status=0');return false"><img
                    src="/img/soc/twitter.png" alt="Twitter"></a>
            <a href="#fb"
               onclick="window.open('http://www.facebook.com/sharer.php?u={{ Request::url() }}', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=200, top=200, width=554, height=421, toolbar=0, status=0');return false"><img
                    src="/img/soc/fb.png" alt="Facebook"></a>
            <a href="#gplus"
               onclick="window.open('https://plus.google.com/share?url='+encodeURIComponent('{{ Request::url() }}'), '_blank', 'scrollbars=0, resizable=1, menubar=0, left=200, top=200, width=554, height=421, toolbar=0, status=0');return false"><img
                    src="/img/soc/gplus.png" alt="Google+"></a>
        </div>
    </div>
</footer>
