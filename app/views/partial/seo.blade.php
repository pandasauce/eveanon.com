<title>
    @section('title')
    EVE Anon {{ isset($title) ? ' - ' . $title : false }}
    @show
</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=1140, initial-scale=1.0">
<meta name="description" content="{{ isset($description) ? strip_tags($description) : 'EVE Anon' }}"/>
<meta name="google-site-verification" content="bnb5QZzD8K9nqIqudR7AHuvEKJsn7vSMuLQY4KwvumI"/>
<meta name="robots" content="{{ App::environment() === 'production' ? 'index, follow' : 'noindex, nofollow' }}">
<meta property="og:locale" content="ru_RU"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="{{ isset($title) ? $title : 'EVE Anon' }}"/>
<meta property="og:description" content="{{ isset($description) ? strip_tags($description) : 'EVE Anon' }}"/>
<meta property="og:url" content="{{ Request::url() }}"/>
<meta property="og:site_name" content="EVE Anon"/>
<meta property="og:image" content="http://eve-anon.com/img/logo.jpg"/>
<link rel="canonical" href="http://eve-anon.com/"/>
<link rel="alternate" type="application/rss+xml" title="EVE Anon - Сайт помощи ньюфагам в EVE Online" href="http://eve-anon.com/atom/"/>
