<header class="navbar navbar-fixed-top navbar-inverse" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapsable">
                <span class="sr-only">Открыть меню</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">EVE Anon</a>
        </div>
        <nav class="collapse navbar-collapse" role="navigation" id="navbar-collapsable">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="/read/2013/09/eve-isk-anon-edition"><img src="/img/ico/isk.png" alt="ISK"> ISK
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/read/2013/09/eve-isk-anon-edition#content">Как заработать на плекс</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#агентран">Агентран</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#вормхолы">Вормхолы</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#воровство">Воровство</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#инкуршены">Инкуршены</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#майнинг">Майнинг</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#ниндзя-сальважинг">Ниндзя-сальважинг</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#производство">Производство</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#сео">СЕО</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#скам">Скам</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#торговля">Торговля</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#суицид">Суицид</a></li>
                        <li>
                            <a href="/read/2013/09/eve-isk-anon-edition#геноцид-неписи-в-нулях">Геноцид неписи в нулях</a>
                        </li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#фракционные-войны">Фракционные войны</a></li>
                        <li><a href="/read/2013/09/eve-isk-anon-edition#эксплорейшн">Эксплорейшн</a></li>
                        <li>
                            <a href="/read/2013/09/eve-isk-anon-edition#скорее-даже-не-экзотические-а-просто-методы-на-которые-жалко-было-выделять-целый-раздел">Экзотика</a>
                        </li>
                    </ul>
                </li>
                <li><a class="dropdown-toggle" data-toggle="dropdown" href="/api/pve"><img src="/img/ico/pve.png" alt="PVE"> PVE
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/api/pve">Поиск ситизен корпорации</a></li>
                        <li><a href="/read/2014/02/kak-polzovatsya-ess">Encounter Surveillance System</a></li>
                        <li><a href="/read/2014/03/kak-prohodit-reliki">Как проходить релики</a></li>
                        <li><a href="/read/2015/02/zarabotok-v-fraktsionnih-voinah">Фракционные Войны</a></li>
                    </ul>
                </li>
                <li><a class="dropdown-toggle" data-toggle="dropdown" href="/read/2014/03/guide-po-pvp"><img src="/img/ico/pvp.png" alt="PVP"> PVP
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/read/2013/10/rasskaz-o-rabstve-v-nulyah">Почему не надо идти в PVP корпорацию</a></li>
                        <li><a href="/read/2014/03/guide-po-pvp">Основы PVP - Овервью, бродкасты, скаутинг и прожимание F1</a></li>
                    </ul>
                </li>
                <li><a  href="/page/alliances"><img src="/img/ico/alliances.png" alt="Альянсы"> Альянсы</a></li>
                <li><a href="/posts/"><img src="/img/ico/guest.png" alt="Гостевые статьи"> Все статьи</a></li>
                {{--<li>--}}
                    {{--<a href="/signatures"><span class="glyphicon glyphicon-pushpin"></span> IGB Signature Tracker</a>--}}
                {{--</li>--}}
                @if(Sentry::check())
                    @include('admin.menu')
                @endif
            </ul>
        </nav>
    </div>
</header>
