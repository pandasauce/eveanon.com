@extends('layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1>CCP Legal Notice</h1>

        <p>
            EVE Online, the EVE logo, EVE and all associated logos and designs are the intellectual property of CCP hf.
            All artwork, screenshots, characters, vehicles, storylines, world facts or other recognizable features of
            the intellectual property relating to these trademarks are likewise the intellectual property of CCP hf. EVE
            Online and the EVE logo are the registered trademarks of CCP hf. All rights are reserved worldwide. All
            other trademarks are the property of their respective owners. CCP hf. has granted permission to EVE Anon to
            use EVE Online and all associated logos and designs for promotional and information purposes on its website
            but does not endorse, and is not in any way affiliated with, EVE Anon. CCP is in no way responsible for the
            content on or functioning of this website, nor can it be liable for any damage arising from the use of this
            website.
        </p>

        <h1>EVE Anon Copyrights Notice</h1>

        <p>
            Копирование <b>текстов</b> сайта допускается, при соблюдении следующих условий:
        </p>

        <p>
            <b>Аттрибуция</b> - Вы должны обеспечить соответствующее указание авторства, предоставить ссылку на
            лицензию, и обозначить изменения, если таковые были сделаны. Вы можете это делать любым разумным способом,
            но не таким, который подразумевал бы, что лицензиар одобряет вас или ваш способ использования произведения.
        </p>

        <p>
            <b>Некоммерческое использование</b> — Вы не вправе использовать этот материал в коммерческих целях.
        </p>

        <p>
            <b>Без производных произведений</b> — Если вы перерабатываете, преобразовываете материал или берёте его за
            основу для производного произведения, вы не можете распространять измененный материал.
        </p>

        <p>
            <b>Без дополнительных ограничений</b> — Вы не вправе применять юридические ограничения или технологические
            меры, создающие другим юридические препятствия в выполнении чего-либо из того, что разрешено лицензией.
        </p>

        <p>
            <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/deed.ru" class="btn btn-default btn-primary"><img
                    src="http://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png"
                    alt="Attribution-NonCommercial-NoDerivatives 4.0 International"> Полный текст лицензии</a>
        </p>

        <p>
            Копирование <b>элементов авторского дизайна, разметки, авторского кода или элементов кода</b> без
            письменного физического разрешения автора не допускается. Это ограничение не распространяется на элементы
            кода, такие как jQuery, созданные и лицензированные другими людьми или организациями.
        </p>

        <p>
            Все вопросы по сотрундичеству, размещению ссылок и копированию материалов можно адресовать по контактным
            данным внизу страницы.
        </p>
    </div>
</div>
@stop