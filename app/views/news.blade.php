@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center"><img src="/img/ico/eveanon.png" alt="EVE Anon"> EVE Anon
                <small>без регистрации и смс</small>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8" id="news-block">
            @foreach($feed['posts'] as $post)
                <h2 class="post-short-title"><a href="{{ $post['link'] }}">{{ $post['short_title'] }}</a></h2>
                <h6 class="post-readable-date clearfix"><span class="pull-left" data-toggle="tooltip" data-placement="right" title="Дата поста">{{ $post['readable_date'] }}</span> <span class="pull-right" data-toggle="tooltip" data-placement="left" title="Автор поста">{{ $post['author'] }}</span></h6>

                <div class="post-wrap">
                    {{ $post['excerpt'] }}
                </div>
            @endforeach
            <p>
                <a href="/posts?page=2" class="btn btn-primary">Более старые записи</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Набор капсулира</h2>
            <dl id="nabor">
                <dd class="alliance-promo clearfix">
                    <div class="alliance-promo-left">
                            <img src="/img/a_oos.png" alt="Out of Sight."/>
                    </div>
                    <h5>Out of Sight.</h5>

                    <p>Best ship is friendship.</p>

                    <p> Лёгкий уклон в дружбомагию,
                        <a href="http://evemaps.dotlan.net/map/Venal">Venal</a> и братишки-анончики в алличате.</p>
                </dd>
                <script>
                    $('.alliance-promo').on('click', function (event) {
                        if (typeof(CCPEVE) != "undefined") {
                            return CCPEVE.showInfo(16159, 99001011);
                        }

                        return window.location.href = "http://evemaps.dotlan.net/alliance/Out_of_Sight.";
                    });
                </script>
                <div class="searchbox">
                    {{ Form::open(['url' => action('EveAnon\Controller\PostController@getSearch'), 'class' => 'searchform', 'method' => 'get']) }}
                    <div class="input-group">
                        <input class="form-control" placeholder="Поиск по сайту" type="text" name="search_keywords" id="search-keywords" maxlength="30"/>
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Вперде</button>
                            </span>
                    </div>
                    {{ Form::close() }}
                </div>
                <dt><a href="http://evemaps.dotlan.net/map" target="_blank"><img src="/img/ico/dotlan.ico" alt="Dotlan"> Dotlan</a></dt>
                <dd>Подробные карты всех регионов евы с информацией по неписи, станциям, лунам, агентам и многое другое.
                </dd>
                <dt>
                    <a href="http://go-dl.eve-files.com/media/corp/verite/influence.png" target="_blank"><img src="/img/ico/verite.ico" alt="Verite"> Verite</a>
                    <a href="http://go-dl1.eve-files.com/media/corp/coalitionsov/Coalitioninfluence.png" target="_blank">[<span style="text-decoration: line-through;">Gay</span> Coalition Edition]</a>
                </dt>
                <dd>Клаймовая карта евы, на которой обозначены сферы влияния <a href="/page/alliances">петушиных
                                                                                                       альянсов</a>.
                </dd>
                <dt><a href="http://eve-central.com/" target="_blank"><img src="/img/ico/central.ico" alt="EVE-Central"> EVE-Central</a>
                </dt>
                <dd>Справочник по ценам хайсечных торговых хабов.</dd>
                <dt><a href="http://evepraisal.com/" target="_blank"><img src="/img/ico/praisal.ico" alt="EVE Praisal"> EVE Praisal</a>
                </dt>
                <dd>Быстрая оценка стоимости лута.</dd>
                <dt>
                    <a href="http://eve-survival.org/wikka.php?wakka=MissionReports" target="_blank"><img src="/img/ico/survival.ico" alt="EVE-Survival">
                        EVE-Survival</a></dt>
                <dd>Прохождение миссий для ньюфагов.</dd>
                <dt><a href="http://wh.pasta.gg/" target="_blank"><img src="/img/ico/wormholes.png" alt="Wormholes"> Wormholes</a></dt>
                <dd>Навигация по узеньким дырочкам.</dd>
                <dt><a href="/atom" target="_blank"><img src="/favicon2.ico" alt="Atom/RSS Feed"> EVE Anon RSS</a></dt>
                <dd>Наш фид. Встраивай в ленту @ читай.</dd>
            </dl>
            <div class="panel panel-default">
                <div class="panel-body"><b>GDE SIDIT ANON?</b> В канале 2ch</div>
            </div>
            <h2><a href="/api/supporters" data-toggle="tooltip" data-placement="bottom" title="Что это у нас тут?">Спонсоры
                                                                                                                   EVE Anon</a>
            </h2>
            {{ $topDonators }}
        </div>
    </div>
@stop
