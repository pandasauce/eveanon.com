@extends('admin.layout')

@section('content')
<div class="row">
<h1>Пользователи и права</h1>
@foreach($users as $user)
    <p>
    <b>{{ $user->email }}</b> - {{ $user->getFullName() }}
    @foreach($user->getMergedPermissions() as $role => $value)
    <br/><i>---- {{ $role }}: {{ $value }}</i>
    @endforeach
    </p>
@endforeach
</div>
@stop
