@extends('admin.layout')

@section('content')
<div class="row">
    <form action="/user/auth" method="POST" class="col-md-6 form-horizontal">
        <legend>У - уходи</legend>
        <div class="form-group">
            <label class="col-sm-2 control-label">Логин</label>

            <div class="col-sm-10">
                <input type="text" name="username" id="username" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">СМС-код</label>

            <div class="col-sm-10">
                <input type="password" name="smscode" id="smscode" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Войти</button>
    </form>
</div>
@stop
