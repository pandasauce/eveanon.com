@extends('layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Как поддержать EVE Anon?</h2>

        <p>
            Обкатывал в своих целях EVE API и решил попутно запустить небольшой <b>эксперимент</b>. Раз в час из <b>EVE
                API</b> выдёргиваются все переводы на валет корпорации <a
                href="https://zkillboard.com/corporation/98252646/" target="_blank" class="btn btn-default btn-xs"><span
                    class="igbinfo"></span> Princess Luna Personal Battalion</a> и составляется список пяти самых щедрых
            анонов - эдакая киллборда с ISK в качестве главной фаллометрической единицы.
        </p>
        <h4>И зачем это нужно?</h4>

        <p style="text-decoration: line-through;">
            Анону - возможность засветиться и померяться количеством лишних исков на валлете, мне - фидбек, мотивация
            больше писать. Если взлетит - переедем на свой ламповый VDS, не взлетит - уберу.
        </p>

        <p>
            На данный момент это просто демонстрация интересной концепции с EVE XML API.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <h2>Главные спонсоры сайта</h2>
        {{ $topDonators }}
    </div>
    <div class="col-md-6">
        <h2>Последние 5 переводов</h2>
        {{ $lastDonators }}
    </div>
</div>
@stop