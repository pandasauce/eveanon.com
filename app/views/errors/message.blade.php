@extends('errors.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1>Что-то пошло не так.</h1>

        <p>{{ $message }}</p>
    </div>
</div>
@stop