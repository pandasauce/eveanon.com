@extends('errors.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1>Ты мне не доверяешь?</h1>

        <p>Нажми <a href="javascript:CCPEVE.requestTrust('{{ url('/') }}')">тута</a> и разреши всё в всплывшем окошке, педик. Потом нажми
            <a href="javascript:location.reload()">здеся</a>.</p>
    </div>
</div>
@stop