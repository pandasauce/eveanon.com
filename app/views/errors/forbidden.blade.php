@extends('errors.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1>Шёл бы ты отсюда.</h1>

        @if (isset($error_message) && $error_message != '')
        <p>{{ $error_message }}</p>
        @else
        <p>Молодой человек, это не для вас сделано.</p>
        @endif
    </div>
</div>
@stop
