<p>
    @foreach($alliances as $alliance)
        <a class="btn btn-default btn-primary" href="javascript:showAlliance('{{ $alliance->getAllianceID() }}');">{{ $alliance->getName() }}</a>
    @endforeach
</p>
@foreach($alliances as $alliance)
    <div class="apiallianceblock" id="{{ $alliance->getAllianceID() }}">

        <h4 class="apiallianceheading">
            <a href="http://evemaps.dotlan.net/alliance/{{ urlencode($alliance->getName()) }}"><span class="igbinfo"></span> {{ $alliance->getName() }}</a>
        </h4>

        <table class="table table-striped table-hover tablesorter apitable">
            <thead>
            <tr>
                <th>Налог <i class="glyphicon glyphicon-sort"></i></th>
                <th>Мемберы <i class="glyphicon glyphicon-sort"></i></th>
                <th>Название <i class="glyphicon glyphicon-sort"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($alliance->getMemberCorporations() as $corporation)
                <tr>
                    <td class="apitaxrate">{{ $corporation->getTaxRate() }}%</td>
                    <td class="apimemcount">{{ $corporation->getMemberCount() }}</td>
                    <td class="apiname" id="{{ $corporation->getCorporationID() }}">
                        <a href="http://evemaps.dotlan.net/corp/{{ urlencode($corporation->getCorporationName()) }}" target="_blank"><span class="igbinfo"></span> {{ $corporation->getCorporationName() }}</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endforeach
