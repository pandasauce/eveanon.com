<!DOCTYPE html>
<html>
<head>
    @include('partial.seo')
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    @section('head')
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    @show
</head>
<body>
<div id="wrap">
    @include('partial.navbar')
    <div class="container">
        <div id="content">
            @yield('content')
        </div>
    </div>
</div>
@include('partial.footer')
<div id="gotop" onclick="document.body.scrollTop = document.documentElement.scrollTop = 0;">↑</div>
@section('scripts')
@show
<script>
    $(window).resize(checkToTop);
    $(document).ready(function () {
        checkToTop();
        $("[data-toggle='tooltip']").tooltip();
        var fdbk = ["mailto", ":", "batya", "@", "eve-anon", ".", "com"];
        for (var i = 0; i < fdbk.length; i++) {
            var t = $('#fdbk').attr('href');
            $('#fdbk').attr('href', t + fdbk[i]);
        }
    });
    function checkToTop() {
        if (window.innerWidth < 788) $('#gotop').fadeOut(1000);
        else $('#gotop').fadeIn(1000);
    }
</script>
</body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-57628695-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');

</script>
</html>
