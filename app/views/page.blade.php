@extends('layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        {{ $content }}
    </div>
</div>
@stop