<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
use EveAnon\Model\Post;
use EveAnon\Model\Signature;
use \EveAnon\Model\System;

// Controllers
Route::controller('posts', 'EveAnon\Controller\PostController');
Route::controller('user', 'EveAnon\Controller\UserController');
Route::resource(
    'signatures',
    'EveAnon\Controller\SignatureController',
    array('except' => array('edit', 'update'))
);
Route::get('signatures/destroy/{systemName}', 'EveAnon\Controller\SignatureController@destroy')
    ->where(array('systemName' => System::$rules['id']));

// Reading posts by slug
Route::get('/read/{year}/{month}/{slug}', 'EveAnon\Controller\PostController@getBySlug')
    ->where(
        array(
            'year'  => '201(0|1|2|3|4|5|6|7|8|9)',
            'month' => '(01|02|03|04|05|06|07|08|09|10|11|12)',
            'slug'  => '[a-z0-9\-]{3,64}'
        )
    );

// Error handlers
App::missing(
    function ($exception) {
        return Response::view('errors.missing', array('title' => '404', 'description' => 'Страница не найдена'), 404);
    }
);

App::error(
    function (\Illuminate\Database\Eloquent\ModelNotFoundException $exception) {
        return Response::view('errors.message', ['message' => 'Запрашиваемого ресурса не существует.'], 404);
    }
);

App::error(
    function (\EveAnon\Exceptions\AccessForbiddenException $exception) {
        return Response::view('errors.forbidden', ['error_message' => $exception->getMessage()], 403);
    }
);

// Legacy routes to refactor
Route::get(
    '/', function () {
        $blog_service = new \EveAnon\Service\BlogService();
        $pageData = array(
            'title'        => 'Новости',
            'description'  => 'Сайт помощи ньюфагам в EVE Online. Как заработать на плекс, 21-дневный триал и обзоры пвп-альянсов.',
            'lastDonators' => file_get_contents(storage_path() . '/pages/api/donationsTable.html'),
            'topDonators'  => file_get_contents(storage_path() . '/pages/api/donationsTop.html'),
            'feed'   => $blog_service->getFeed(),
        );

        return View::make('news', $pageData);
    }
);

Route::get(
    'atom', function () {
        $blog_service = new \EveAnon\Service\BlogService();

        return $blog_service->getAtom();
    }
);

Route::get(
    'trial', function () {
        $pageData = array(
            'title'       => 'Пробный аккаунт на 21 день',
            'description' => 'Как получить пробный аккаунт в EVE Online на 21 день без наёба'
        );

        return View::make('trial', $pageData);
    }
);

Route::get(
    'copyrights', function () {
        $pc = array(
            'title'       => 'Формальности',
            'description' => 'CCP Legal Notice and Copyright notes'
        );

        return View::make('copyrights', $pc);
    }
);

Route::get(
    'api/supporters', function () {
        $pc = array(
            'title'        => 'Спонсоры показа',
            'description'  => 'Рейтинг людей, оказавших материальную поддержку ресурсу EVE Anon.',
            'lastDonators' => file_get_contents(storage_path() . '/pages/api/donationsTable.html'),
            'topDonators'  => file_get_contents(storage_path() . '/pages/api/donationsTop.html')
        );

        return View::make('supporters', $pc);
    }
);

Route::get(
    'api/pve', function () {
        $pc = array(
            'title'       => 'Сравнение каребирских корпораций',
            'description' => 'Сравнение каребирских корпораций в EVE Online по налогу и количеству мемберов',
            'corpsTable'  => file_get_contents(storage_path() . '/pages/api/corpsTable.html')
        );

        return View::make('goodcorps', $pc);
    }
);
