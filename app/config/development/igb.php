<?php

return [
    /* This header will have the value “Yes”. */
    'EVE_TRUSTED'           => 'Yes',
    /* This header will have a string containing the IP address of the server to which the end user is currently connected. */
    'EVE_SERVERIP'          => '0.0.0.0',
    /* This header will have a string containing the end user’s character’s name. */
    'EVE_CHARNAME'          => 'Innately Awesome',
    /* This header will have an integer containing the end user’s character ID number. This number is unique to each character in EVE. */
    'EVE_CHARID'            => '91360444',
    /* This header will have a string containing the end user’s corporation’s name. */
    'EVE_CORPNAME'          => 'ATAC OTMOPO3OK B KOCMOCE',
    /* This header will have an integer containing the end user’s corporation ID number. This number is unique to each corporation in EVE. */
    'EVE_CORPID'            => '98234945',
    /* This header will have a string containing the end user’s alliance name. If the character is not in an alliance, this header will be set to "None". */
    'EVE_ALLIANCENAME'      => 'Northern Associates.',
    /* This header will have an integer containing the end user’s alliance ID number. This number is unique to each alliance in EVE. If the character is not in an alliance, this header will be set to "None". */
    'EVE_ALLIANCEID'        => '99000163',
    /* This header will contain the name of the region in which the end user’s character is presently located. */
    'EVE_REGIONNAME'        => 'Paragon Soul',
    /* This header will contain the name of the constellation in which the end user’s character is presently located. */
    'EVE_CONSTELLATIONNAME' => 'D9DM-O',
    /* This header will contain the name of the solar system in which the end user’s character is presently located. */
    'EVE_SOLARSYSTEMNAME'   => 'MP5-KR',
    /* This header will contain the name of the station in which the end user’s character is presently located. If the character is in space, or otherwise not at a station, this header will be set to "None". */
    'EVE_STATIONNAME'       => 'Pleru was here',
    /* This header will contain the unique identifier number assigned to the station at which the user’s character is presently located. If the character is in space, or otherwise not in a station, this header will not be present. *Note* This header does not appear to be used. */
    'EVE_STATIONID'         => '',
    /* This header will contain an integer which is the set of corporation role bits combined via binary-OR operations that have been granted to the current user’s character by his/her corporation. If the character does not have any roles, this will be the number zero. */
    'EVE_CORPROLE'          => '',
    /* This header will contain the id of the solar system in which the end user’s character is presently located. */
    'EVE_SOLARSYSTEMID'     => 'MP5-KR',
    /* This header will contain the users warfaction id */
    'EVE_WARFACTIONID'      => '',
    /* This header will contain the users ship id */
    'EVE_SHIPID'            => '',
    /* This header will contain the users ship name */
    'EVE_SHIPNAME'          => '',
    /* This header will contain the users ship type id */
    'EVE_SHIPTYPEID'        => '',
    /* This header will contain the users ship type nam  */
    'EVE_SHIPTYPENAME'      => '',
];
