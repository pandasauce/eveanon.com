<?php

return [

    'debug'     => true,
    'aliases'   => append_config(
        [
            'Debugbar' => 'Barryvdh\Debugbar\Facade'
        ]
    ),
    'providers' => append_config(
        [
            'Way\Generators\GeneratorsServiceProvider',
            'Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider',
            'Barryvdh\Debugbar\ServiceProvider',
        ]
    ),
];
