<?php
namespace EveAnon\Model;

use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;

/**
 * EveAnon\Model\User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $permissions
 * @property boolean $activated
 * @property string $activation_code
 * @property \Carbon\Carbon $activated_at
 * @property \Carbon\Carbon $last_login
 * @property string $persist_code
 * @property string $reset_password_code
 * @property string $first_name
 * @property string $last_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\EveAnon\Model\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\static::$groupModel[] $groups
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereActivated($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereActivationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereActivatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User wherePersistCode($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereResetPasswordCode($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\User whereUpdatedAt($value)
 */
class User extends SentryUserModel
{
    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function posts()
    {
        return $this->hasMany('EveAnon\Model\Post');
    }
}
