<?php
namespace EveAnon\Model;

use EveAnon\Service\Igb;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * EveAnon\Model\Corporation
 *
 * @property integer $id
 * @property string $name
 * @property integer $alliance_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \EveAnon\Model\Alliance $alliance
 * @property-read \Illuminate\Database\Eloquent\Collection|\EveAnon\Model\Character[] $characters
 * @property-read \Illuminate\Database\Eloquent\Collection|\EveAnon\Model\Signature[] $signatures
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Corporation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Corporation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Corporation whereAllianceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Corporation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Corporation whereUpdatedAt($value)
 */
class Corporation extends BaseModel
{
    public $incrementing = false;

    /**
     * @param Igb $igb
     *
     * @return static
     */
    public static function findOrMake(Igb $igb)
    {
        /** @var static $entity */
        $id = $igb->header('EVE_CORPID');
        try {
            $entity = static::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $entity = new static;
            $entity->id = $id;
            $entity->name = $igb->header('EVE_CORPNAME');
            $entity->save();
        }
        if ($igb->header('EVE_ALLIANCEID') != '') {
            $alliance = Alliance::findOrMake($igb);
            $alliance->corporations()->save($entity);
        }

        return $entity;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function alliance()
    {
        return $this->belongsTo('EveAnon\Model\Alliance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function characters()
    {
        return $this->hasMany('EveAnon\Model\Character');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function signatures()
    {
        return $this->belongsToMany('EveAnon\Model\Signature')->withTimestamps();
    }
}
