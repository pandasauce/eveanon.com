<?php
namespace EveAnon\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;

/**
 * EveAnon\Model\Post
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $shortTitle
 * @property string $longTitle
 * @property string $postDescription
 * @property string $slug
 * @property string $rawHtml
 * @property bool $boolPublic
 * @property bool $boolComments
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \EveAnon\Model\User $user
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereShortTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereLongTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post wherePostDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereRawHtml($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereBoolPublic($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereBoolComments($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Post whereUpdatedAt($value)
 * @method static \EveAnon\Model\Post byDate()
 * @method static \EveAnon\Model\Post public()
 */
class Post extends BaseModel
{
    protected $fillable = ['shortTitle', 'longTitle', 'postDescription', 'picUrl', 'slug', 'postDate', 'rawHtml', 'boolPublic', 'boolComments', 'created_at'];

    /* @return User */
    public function user()
    {
        return $this->belongsTo('EveAnon\Model\User');
    }

    /**
     * @param QueryBuilder|Builder $query
     *
     * @return QueryBuilder|Builder|static
     */
    public function scopeByDate($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * @param QueryBuilder|Builder $query
     *
     * @return QueryBuilder|Builder|static
     */
    public function scopePublic($query)
    {
        return $query->where('boolPublic', '=', true);
    }
}
