<?php
namespace EveAnon\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Database\Query\Builder as QueryBuilder;

/**
 * EveAnon\Model\Signature
 *
 * @property string $id
 * @property string $scan_group
 * @property string $group
 * @property string $type
 * @property float $signal
 * @property string $distance
 * @property string $system_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \EveAnon\Model\System $system
 * @property-read \Illuminate\Database\Eloquent\Collection|\EveAnon\Model\Corporation[] $corporations
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereScanGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereSignal($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereDistance($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereSystemId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Signature whereDeletedAt($value)
 * @method static \EveAnon\Model\Signature ofSystem($system)
 */
class Signature extends BaseModel
{
    public $incrementing = false;
    use SoftDeletingTrait;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function system()
    {
        return $this->belongsTo('EveAnon\Model\System');
    }

    /**
     * Update signature data from another signature.
     *
     * @param Signature $signature
     */
    public function updateFrom(Signature $signature)
    {
        $this->scan_group = $signature->scan_group;
        $this->group = $signature->group;
        $this->type = $signature->type;
    }

    /**
     * @param QueryBuilder|Builder $query
     * @param System               $system
     *
     * @return QueryBuilder|Builder
     */
    public static function scopeOfSystem($query, System $system)
    {
        return $query->where('system_id', '=', $system->id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder
     */
    public function corporations()
    {
        return $this->belongsToMany('EveAnon\Model\Corporation')->withTimestamps();
    }
}
