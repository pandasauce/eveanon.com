<?php
namespace EveAnon\Model;

use EveAnon\Service\Igb;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * EveAnon\Model\Alliance
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\EveAnon\Model\Corporation[] $corporations
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Alliance whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Alliance whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Alliance whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Alliance whereUpdatedAt($value)
 */
class Alliance extends BaseModel
{
    public $timestamps = false;
    public $incrementing = false;

    /**
     * @param Igb $igb
     *
     * @return static
     */
    public static function findOrMake(Igb $igb)
    {
        $id = $igb->header('EVE_ALLIANCEID');
        try {
            $entity = static::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $entity = new static;
            $entity->id = $id;
            $entity->name = $igb->header('EVE_ALLIANCENAME');
            $entity->save();
        }

        return $entity;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function corporations()
    {
        return $this->hasMany('EveAnon\Model\Corporation');
    }
}
