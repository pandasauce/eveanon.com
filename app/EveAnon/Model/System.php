<?php
namespace EveAnon\Model;

use EveAnon\Service\Igb;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * EveAnon\Model\System
 *
 * @property string $id
 * @property string $constellation_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\EveAnon\Model\Signature[] $signatures
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\System whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\System whereConstellationId($value)
 * @method static \EveAnon\Model\System notEmpty()
 */
class System extends BaseModel
{
    /* @var array Rules for validation */
    public static $rules = [
        'id' => '[A-Za-z0-9\-\ ]+',
    ];
    public $timestamps = false;
    public $incrementing = false;

    /**
     * @param Igb $igb
     *
     * @return static
     */
    public static function findOrMake(Igb $igb)
    {
        $id = $igb->header('EVE_SOLARSYSTEMNAME');
        try {
            $entity = static::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $entity = new static;
            $entity->id = $id;
            $entity->save();
        }

        return $entity;
    }

    /**
     * @param string $id
     *
     * @return static
     */
    public static function findOrMakeById($id)
    {
        try {
            $entity = static::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $entity = new static;
            $entity->id = $id;
            $entity->save();
        }

        return $entity;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder|Builder
     */
    public function signatures()
    {
        return $this->hasMany('EveAnon\Model\Signature');
    }

    /**
     * Deletes all signatures associated with the system.
     */
    public function purge()
    {
        $this->signatures()->forceDelete();
    }

    /**
     * @param Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopeNotEmpty($query)
    {
        return $query->with('signatures');
    }
}
