<?php
namespace EveAnon\Model;

use EveAnon\Service\Igb;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * EveAnon\Model\Character
 *
 * @property integer $id
 * @property string $name
 * @property integer $points
 * @property integer $corporation_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \EveAnon\Model\Corporation $corporation
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Character whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Character whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Character wherePoints($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Character whereCorporationId($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Character whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EveAnon\Model\Character whereUpdatedAt($value)
 */
class Character extends BaseModel
{
    public $incrementing = false;

    /**
     * @param Igb $igb
     *
     * @return static
     */
    public static function findOrMake(Igb $igb)
    {
        $id = $igb->header('EVE_CHARID');
        try {
            $entity = static::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $entity = new static;
            $entity->id = $id;
            $entity->name = $igb->header('EVE_CHARNAME');
            if ($igb->header('EVE_CORPID') != '') {
                $corporation = Corporation::findOrMake($igb);
                $corporation->characters()->save($entity);
            }
            $entity->save();
        }

        return $entity;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Corporation
     */
    public function corporation()
    {
        return $this->belongsTo('EveAnon\Model\Corporation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Signature[]
     */
    public function signatures()
    {
        return $this->corporation()->signatures();
    }

    /**
     * Increase points that a character has by a given amount.
     *
     * @param int $amount
     */
    public function increasePoints($amount = 1)
    {
        $this->points += $amount;
        $this->save();
    }
}
