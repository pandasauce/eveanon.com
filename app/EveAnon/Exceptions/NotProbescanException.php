<?php
namespace EveAnon\Exceptions;

use Exception;

class NotProbescanException extends Exception
{
    public function __construct($message = 'Supplied string was not a valid probescan', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
 