<?php
namespace EveAnon\Exceptions;

use Exception;

class AccessForbiddenException extends Exception
{
    public function __construct($message = 'Вам закрыт доступ к этой странице.', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
