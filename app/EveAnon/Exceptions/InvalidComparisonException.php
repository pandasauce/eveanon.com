<?php
namespace EveAnon\Exceptions;

use Exception;

class InvalidComparisonException extends Exception
{
    public function __construct($message = 'There is no point in comparing two signatures with different IDs', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
 