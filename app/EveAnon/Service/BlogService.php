<?php
namespace EveAnon\Service;

use EveAnon\Exceptions\AccessForbiddenException;
use EveAnon\Model\Post;
use EveAnon\Model\User;
use Feed;
use Sentry;
use URL;

class BlogService
{
    /**
     * @return \View
     */
    public function getAtom()
    {
        $feed = Feed::make();

        $feed->setCache(60, 'atomFeed');

        if (!$feed->isCached()) {
            $feed_posts = $this->getFeed();
            $feed->title = 'EVE Anon - Сайт помощи ньюфагам в EVE Online';
            $feed->description = 'Новости EVE Online от анонимуса. Как заработать на плекс, 21-дневный триал и обзоры пвп-альянсов.';
            $feed->logo = 'http://eve-anon.com/img/logo.jpg';
            $feed->link = URL::to('feed');
            $feed->setDateFormat('datetime');
            $feed->pubdate = $feed_posts['posts'][0]['raw_date'];
            $feed->lang = 'ru';

            foreach ($feed_posts['posts'] as $post) {
                $feed->add(
                    $post['short_title'],
                    $post['author'],
                    $post['link'],
                    $post['raw_date'],
                    $post['page_description'],
                    $post['excerpt']
                );
            }
        }

        return $feed->render('atom');
    }

    /**
     * @param bool $only_published
     *
     * @return array
     */
    public function getFeed($only_published = true)
    {
        $paged_posts = $this->fetchPosts($only_published);

        $feed_posts = [
            'posts' => $this->prepareFeed($paged_posts),
            'links' => $paged_posts->links()
        ];

        return $feed_posts;
    }

    /**
     * @param bool $only_published
     *
     * @return Post[]|\Illuminate\Pagination\Paginator
     */
    protected function fetchPosts($only_published)
    {
        $query = Post::byDate();
        if ($only_published) {
            $query->public();
        }

        $paged_posts = $query->paginate(5);

        return $paged_posts;
    }

    /**
     * @param Post[] $raw_posts
     *
     * @return array
     */
    protected function prepareFeed($raw_posts)
    {
        $prepared_posts = [];

        foreach ($raw_posts as $post) {
            $prepared_posts[] = $this->preparePost($post);
        }

        return $prepared_posts;
    }

    /**
     * @param Post $post
     *
     * @return array
     */
    protected function preparePost(Post $post)
    {
        $post_data = [
            'link'             => $this->url($post),
            'short_title'      => $post->shortTitle,
            'long_title'       => $post->longTitle,
            'slug'             => $post->slug,
            'page_title'       => $post->shortTitle,
            'page_description' => $post->postDescription,
            'readable_date'    => $this->readableDate($post),
            'raw_date'         => $post->created_at,
            'author'           => $post->user->first_name,
            'excerpt'          => $this->excerpt($post),
            'full'             => $post->rawHtml,
            'bool_comments'    => $post->boolComments,
            'bool_public'      => $post->boolPublic,
        ];

        return $post_data;
    }

    /**
     * @param Post $post
     *
     * @return string
     */
    protected function url(Post $post)
    {
        return '/read/' . $this->yearMonthSlug($post);
    }

    /**
     * @param Post $post
     *
     * @return string
     */
    protected function yearMonthSlug(Post $post)
    {
        $yearMonth = substr($post->created_at, 0, 8);
        $yearMonth = str_replace('-', '/', $yearMonth);

        return $yearMonth . $post->slug;
    }

    /**
     * @param Post $post
     *
     * @return mixed|string
     */
    protected function readableDate(Post $post)
    {
        // Взять читаемую дату
        $readableDate = strftime('%d %B %Y, %H:%M', strtotime($post->created_at));
        // Перевести на русич
        $monthsEn = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $monthsRu = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
        $readableDate = str_replace($monthsEn, $monthsRu, $readableDate);
        // Прицепить админку, @todo decouple sentry
        if (Sentry::check()) {
            $readableDate .= ' <a href="/posts/edit/' . $post->id . '">Редактировать</a> <a href="/posts/delete/' . $post->id . '">[X]</a>';
        }

        return $readableDate;
    }

    /**
     * @param Post $post
     *
     * @return string
     */
    protected function excerpt(Post $post)
    {
        $split = strstr($post->rawHtml, '<!-- more -->', true);
        if (is_string($split)) {
            $split .= '<p><br/><a class="btn btn-default" href="' . $this->url($post) . '">Читать весь пост</a></p>';
        } else {
            $split = $post->rawHtml;
        }

        return $split;
    }

    /**
     * @param int $post_id
     *
     * @return array
     */
    public function getPost($post_id)
    {
        /** @var Post $post */
        $post = Post::find($post_id);

        return $this->preparePost($post);
    }

    /**
     * @param int  $post_id
     * @param User $actor
     *
     * @throws AccessForbiddenException
     * @return bool|Post
     */
    public function getPostForEdit($post_id, User $actor)
    {
        /** @var Post $post */
        $post = Post::find($post_id);
        if (!$post) {
            return false;
        } else {
            // If post belongs to the user or user is admin
            $this->ownOrAdminIdCheck($post->user_id, $actor);

            return $post;
        }
    }

    /**
     * @param int  $supplied_id
     * @param User $actor
     *
     * @throws AccessForbiddenException
     */
    protected function ownOrAdminIdCheck($supplied_id, User $actor)
    {
        if ($supplied_id !== $actor->id && !$actor->hasAccess('web.admin')) {
            throw new AccessForbiddenException('Можно редактировать только свои посты.');
        }
    }

    /**
     * @param string $year
     * @param string $month
     * @param string $slug
     *
     * @return array
     */
    public function getPostBySlug($year, $month, $slug)
    {
        /** @var Post $post */
        $post = Post::where('created_at', 'like', '%' . $year . '-' . $month . '%')->where('slug', '=', $slug)->firstOrFail();

        return $this->preparePost($post);
    }

    /**
     * @param int  $post_id
     * @param User $actor
     *
     * @throws AccessForbiddenException
     * @throws \Exception
     * @return bool|null
     */
    public function deletePost($post_id, User $actor)
    {
        /** @var Post $post */
        $post = Post::findOrFail($post_id);
        $this->ownOrAdminIdCheck($post->user_id, $actor);

        return $post->delete();
    }

    /**
     * @return Post
     */
    public function generateDummyPost()
    {
        $dummy = new Post;
        $dummy->shortTitle = 'Новый пост';
        $dummy->longTitle = 'Новый пост';
        $dummy->postDescription = 'Новая запись о чём-то интересном';
        //$dummy->picUrl = "/img/posts/default.png";
        $dummy->slug = 'novyi-post';
        $dummy->rawHtml = 'Текст поста';
        $dummy->boolPublic = true;
        $dummy->boolComments = true;
        $dummy->created_at = date('Y-m-d H:i:s');

        return $dummy;
    }

    /**
     * @param array $post_data
     * @param User  $actor
     *
     * @return int
     */
    public function writePost(array $post_data, User $actor)
    {
        $post = new Post($post_data);
        $actor->posts()->save($post);

        return $post->id;
    }

    /**
     * @param int   $post_id
     * @param array $post_data
     * @param User  $actor
     *
     * @return int
     * @throws AccessForbiddenException
     */
    public function editPost($post_id, array $post_data, User $actor)
    {
        if (!isset($post_data['boolPublic'])) {
            $post_data['boolPublic'] = false;
        }
        if (!isset($post_data['boolComments'])) {
            $post_data['boolComments'] = false;
        }
        /** @var Post $post */
        $post = Post::findOrFail($post_id);
        $this->ownOrAdminIdCheck($post->user_id, $actor);
        $post->fill($post_data);
        $post->save();

        return $post->id;
    }

    /**
     * @param string $keywords
     *
     * @return array
     */
    public function searchFeed($keywords)
    {
        $query = Post::byDate()
            ->public()
        ->where('rawHtml', 'LIKE', '%' . $keywords . '%');

        $paged_posts = $query->paginate(5)
                             ->appends(['search_keywords' => $keywords]);

        $feed_posts = [
            'posts' => $this->prepareFeed($paged_posts),
            'links' => $paged_posts->links()
        ];

        return $feed_posts;
    }
}
