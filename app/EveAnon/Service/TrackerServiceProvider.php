<?php
namespace EveAnon\Service;

use EveAnon\Exceptions\InvalidComparisonException;
use Illuminate\Support\ServiceProvider;
use Response;

class TrackerServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'SignatureTracker',
            function () {
                return new SignatureTracker(new ScanParser(), new Igb());
            }
        );
        //        @todo find out how to inject same object to many services
        $this->app->bind(
            'Igb',
            function () {
                return new Igb();
            }
        );

        $this->app->error(
            function (InvalidComparisonException $exception) {
                return Response::view('errors.message', array('message' => $exception->getMessage()), 500);
            }
        );
    }
}
