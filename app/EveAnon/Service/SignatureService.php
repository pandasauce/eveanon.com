<?php
namespace EveAnon\Service;

use Carbon\Carbon;
use EveAnon\Model\Character;
use EveAnon\Model\Signature;
use EveAnon\Model\System;

class SignatureService
{
    protected $igb, $signatureTracker;

    /**
     * @param Igb              $igb
     * @param SignatureTracker $signatureTracker
     */
    public function __construct(Igb $igb, SignatureTracker $signatureTracker)
    {
        $this->igb = $igb;
        $this->signatureTracker = $signatureTracker;
    }

    /**
     * @return System
     */
    public function getCurrentSystem()
    {
        return System::findOrMake($this->igb);
    }

    /**
     * @param string $systemName
     *
     * @return System
     */
    public function getSystem($systemName)
    {
        return System::findOrMakeById($systemName);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|System[]
     */
    public function getSignaturesInAllSystems()
    {
        if ($this->igb->isAdminMode()) {
            $signatures = Signature::all();
        } else {
            $corporation = Character::findOrMake($this->igb)->corporation;
            $signatures = $corporation->signatures()->get();
        }

        return $signatures;
    }

    /**
     * @return Character
     */
    public function getCurrentCharacter()
    {
        return Character::findOrMake($this->igb);
    }

    /**
     * @param string $systemName
     */
    public function purge($systemName)
    {
        $system = System::findOrMakeById($systemName);
        $system->purge();
    }

    /**
     * @param string    $input
     * @param System    $system
     * @param Character $character
     *
     * @return mixed
     */
    public function handleScan($input, System $system, Character $character)
    {
        return $this->signatureTracker->handleScan($input, $system, $character);
    }

    /**
     * @param System $system
     *
     * @return \EveAnon\Model\Signature[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getSignaturesInSystem(System $system)
    {
        if ($this->igb->isAdminMode()) {
            $signatures = $system->signatures;
        } else {
            $character = Character::findOrMake($this->igb);
            $corporation = $character->corporation;
            // Signatures belonging to this character's corp in this system
            $signatures = $corporation->signatures()->ofSystem($system)->get();
        }

        return $signatures;
    }

    /**
     * @param int $maxHours
     *
     * @return int
     */
    public function purgeOld($maxHours = 48)
    {
        $cutoff_time = Carbon::now()->subHours($maxHours);
        $deleted_hard = Signature::where('created_at', '<', $cutoff_time)->withTrashed()->forceDelete();

        return $deleted_hard;
    }

    /**
     * Hides signatures made before last today's downtime, deleted signatures made before yesterday's downtime
     *
     * @return int
     */
    public function purgeDowntime()
    {
        // Real delete yesterday's DT
        $downtime_yesterday = Carbon::yesterday()->hour(11)->minute(5);
        $deleted_hard = Signature::where('created_at', '<', $downtime_yesterday)->withTrashed()->forceDelete();

        // Soft delete today's DT
        $downtime_today = Carbon::today()->hour(11)->minute(5);
        $deleted_soft = Signature::where('created_at', '<', $downtime_today)->delete();

        return ($deleted_hard + $deleted_soft);
    }

    /**
     * @return int
     */
    public function getTotalActiveCount()
    {
        return Signature::count();
    }
}
