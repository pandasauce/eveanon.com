<?php
namespace EveAnon\Service;

use App;
use Config;

class Igb
{
    /**
     * Returns a value from the IGB headers by key
     *
     * @param string $field
     *
     * @return mixed
     */
    public function header($field = '')
    {
        if (App::environment() != 'production') {
            // Dummy data for testing and dev
            $headers = Config::get('igb');
        } else {
            // $_SERVER does not always get populated correctly
            $headers = apache_request_headers();
        }
        // hosting hack - sort your apache instance out guys
        foreach ($headers as $key => $value) {
            $headers[str_replace('-', '_', (strtoupper($key)))] = $value;
        }
        // Simple key=>value
        if (!isset($headers[$field])) {
            return false;
        } else {
            return $headers[$field];
        }
    }

    /**
     * @return bool
     */
    public function isAdminMode()
    {
        if (App::environment() != 'production') {
            // Always admin mode for dev
            return true;
        } else {
            if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || in_array($this->header('EVE_CHARNAME'), Config::get('evefriends'))) {
                return true;
            } else {
                return false;
            }
        }
    }
} 
