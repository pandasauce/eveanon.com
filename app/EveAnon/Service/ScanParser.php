<?php
namespace EveAnon\Service;

use EveAnon\Exceptions\NotProbescanException;
use EveAnon\Model\Signature;

class ScanParser
{

    /**
     * Parses probe scanner copypasta and returns array of Signature objects.
     *
     * @param string $stringToParse
     *
     * @throws \EveAnon\Exceptions\NotProbescanException
     * @return Signature[]
     */
    public function probescan($stringToParse = '')
    {
        $signatures = array();
        $lines = explode("\r\n", $stringToParse);
        if (count($lines) == 0 || $stringToParse == '') {
            throw new NotProbescanException;
        }
        foreach ($lines as $line) {
            $signatures[] = $this->signature($line);
        }

        return $signatures;
    }

    /**
     * Parses a line from probe scanner copypasta
     *
     * @param string $line
     *
     * @return Signature
     */
    public function signature($line = '')
    {
        /* Sample data
        |----------------------------------------------------------------------------
        | ID		Scan Group		Group		Type				Signal	Distance
        |----------------------------------------------------------------------------
        | DVU-725	Cosmic Anomaly	Combat Site	Drone Horde	100.00%	4.79 AU
        | OTG-632	Cosmic Anomaly	Combat Site	Sansha Forsaken Hub	100.00%	5.33 AU
        | GKF-850	Cosmic Anomaly	Combat Site	Drone Surveillance	100.00%	3.61 AU
        | EKF-692	Cosmic Anomaly	Combat Site	Sansha Hidden Rally Point	100.00%	5.25 AU
        | UIF-848	Cosmic Anomaly	Combat Site	Sansha Hidden Hub	100.00%	8.46 AU
        | VKF-035	Cosmic Anomaly	Combat Site	Sansha Forsaken Rally Point	100.00%	1.07 AU
        | DKF-613	Cosmic Anomaly	Combat Site	Sansha Forlorn Hub	100.00%	5.28 AU
        | BGI-354	Cosmic Anomaly	Combat Site	Sansha Hub	100.00%	3.16 AU
        | ZLF-405	Cosmic Anomaly	Combat Site	Sansha Forsaken Rally Point	100.00%	4.42 AU
        | AJF-322	Cosmic Anomaly	Combat Site	Sansha Forlorn Rally Point	100.00%	1.85 AU
        | LFF-020	Cosmic Anomaly	Combat Site	Sansha Forsaken Hub	100.00%	2.27 AU
        | ZIF-243	Cosmic Anomaly	Combat Site	Sansha Forsaken Den	100.00%	4.20 AU
        | TKF-877	Cosmic Anomaly	Combat Site	Sansha Haven	100.00%	9.98 AU
        | VJF-981	Cosmic Anomaly	Combat Site	Sansha Forlorn Den	100.00%	6.43 AU
        | NLA-372	Cosmic Signature			0%	8.58 AU
        */
        $array = explode("\t", $line);
        if (count($array) < 6) {
            throw new NotProbescanException;
        }
        $signature = new Signature;
        $signature->id = $array[0];
        $signature->scan_group = $array[1];
        $signature->group = $array[2];
        $signature->type = $array[3];
        $signature->signal = $array[4];
        $signature->distance = $array[5];

        return $signature;
    }
}
