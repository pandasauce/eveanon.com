<?php
namespace EveAnon\Service;

use Illuminate\Database\Query\Builder;
use EveAnon\Exceptions\InvalidComparisonException;
use EveAnon\Exceptions\NotProbescanException;
use EveAnon\Model\Character;
use EveAnon\Model\Signature;
use EveAnon\Model\System;
use Illuminate\Support\Collection;

class SignatureTracker
{
    public $parser;

    /**
     * @param ScanParser $parser
     */
    public function __construct(ScanParser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Parses the probe scanner, updates the database, tells if any data was useful.
     *
     * @param string    $strScan
     * @param System    $system
     * @param Character $character
     *
     * @return int
     */
    public function handleScan($strScan, System $system, Character $character)
    {
        $points = 0;

        // Parse the input
        try {
            $scannedSignatures = $this->parser->probescan($strScan);
        } catch (NotProbescanException $e) {
            return 0;
        }

        $knownSignatures = $system->signatures;

        $usefulness = $this->isUsefulScan($knownSignatures, $scannedSignatures, $system);
        if (is_numeric($usefulness) && $usefulness < 0) {
            $points = 1;
            $character->increasePoints($points);
        } elseif ($usefulness instanceof Collection && $usefulness->count() > 0) {
            $points = $usefulness->count();
            $character->increasePoints($points);
            $corporation = $character->corporation;
            foreach ($usefulness as $signature) {
                /** @var Signature $signature */
                // Is it already attached?
                if ($signature->whereHas(
                        'corporations', function ($query) use ($corporation, $signature) {
                            /** @var Builder $query */
                            $query->where('corporation_id', '=', $corporation->id)->where('signature_id', '=', $signature->id);
                        }
                    )->count() > 0
                ) {
                    continue;
                } else {
                    $signature->corporations()->attach($corporation);
                }
            }
        }

        return $points;
    }

    /**
     * @param $knownSignatures
     * @param $scannedSignatures
     * @param $system
     *
     * @return Collection|int|Signature[] True if any data was useful
     */
    protected function isUsefulScan($knownSignatures, $scannedSignatures, $system)
    {
        // Handle the input and determine if it was useful or not
        $deleteResult = $this->deleteOld($knownSignatures, $scannedSignatures);
        $updateResult = $this->updateNew($knownSignatures, $scannedSignatures, $system);

        if ($updateResult->count() > 0) {
            return $updateResult;
        } elseif ($deleteResult) {
            return -1;
        } else {
            return false;
        }
    }

    /**
     * Compare scanned signatures to known and delete those which no longer exist.
     *
     * @param Signature[] $knownSignatures
     * @param Signature[] $scannedSignatures
     *
     * @return int
     */
    protected function deleteOld($knownSignatures, $scannedSignatures)
    {
        $useful = 0;
        foreach ($knownSignatures as $knownSignature) {
            if (!$this->isAliveSignature($scannedSignatures, $knownSignature)) {
                $knownSignature->forceDelete();
                $useful++;
            }
        }

        return $useful;
    }

    /**
     * @param Signature[] $scannedSignatures
     * @param Signature   $knownSignature
     *
     * @return bool
     */
    protected function isAliveSignature($scannedSignatures, Signature $knownSignature)
    {
        foreach ($scannedSignatures as $scannedSignature) {
            if ($scannedSignature->id == $knownSignature->id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Compare signatures to known. Update/insert if new data is found.
     *
     * @param Signature[] $knownSignatures
     * @param Signature[] $scannedSignatures
     * @param System      $system
     *
     * @return Collection|Signature[] New or updated signatures
     */
    protected function updateNew($knownSignatures, $scannedSignatures, System $system)
    {
        $useful = [];
        foreach ($scannedSignatures as $scannedSignature) {
            if ($knownSignature = $this->isKnownSignature($knownSignatures, $scannedSignature)) {
                if ($this->compare($knownSignature, $scannedSignature) > 0) {
                    $knownSignature->updateFrom($scannedSignature);
                    $knownSignature->save();
                    $useful[] = $knownSignature;
                }
            } else {
                $system->signatures()->save($scannedSignature);
                $useful[] = $scannedSignature;
            }
        }

        return new Collection($useful);
    }

    /**
     * @param Signature[] $knownSignatures
     * @param Signature   $scannedSignature
     *
     * @return bool|Signature
     */
    protected function isKnownSignature($knownSignatures, Signature $scannedSignature)
    {
        foreach ($knownSignatures as $knownSignature) {
            if ($knownSignature->id == $scannedSignature->id) {
                return $knownSignature;
            }
        }

        return false;
    }

    /**
     * Compare two signatures.
     * Negative if A had more data than B.
     * Positive if A had less data than B.
     * Zero if had equal amounts of data.
     *
     * @param Signature $a
     * @param Signature $b
     *
     * @throws \EveAnon\Exceptions\InvalidComparisonException
     * @return int
     */
    protected function compare(Signature $a, Signature $b)
    {
        if ($a->id != $b->id) {
            throw new InvalidComparisonException;
        }
        $points = ['a' => 0, 'b' => 0];
        $attributes_models = ['a' => $a->getAttributes(), 'b' => $b->getAttributes()];
        $attributes = ['id', 'scan_group', 'group', 'type'];
        foreach ($attributes as $attribute) {
            if ($attributes_models['a'][$attribute] != '' && $attributes_models['b'][$attribute] == '') {
                /* A has, B has not, A > B */
                $points['a']++;
            } elseif ($attributes_models['a'][$attribute] == '' && $attributes_models['b'][$attribute] != '') {
                /* A has not, B has, A < B */
                $points['b']++;
            }
        }

        return ($points['b'] - $points['a']);
    }
}
