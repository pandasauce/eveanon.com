<?php

namespace EveAnon\Controller;

use App;
use Exception;
use Input;
use Redirect;
use Response;
use Sentry;
use View;

class UserController extends BaseController
{
    public function __construct()
    {
        /*if (App::environment() === 'production') {
            $this->beforeFilter('force.ssl');
		}*/
        $this->beforeFilter('web.admins', ['only' => ['getList']]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        return View::make('admin.login', array('title' => 'У - Уходи', 'description' => 'Молодой человек, это не для вас сделано.'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function postAuth()
    {
        try {
            $credentials = array(
                'email'    => Input::get('username'),
                'password' => Input::get('smscode'),
            );
            $user = Sentry::authenticate($credentials, true);

            return Redirect::action('EveAnon\Controller\PostController@getEdit');
        } catch (Exception $e) {
            return Response::view('errors.generic', array(), 401);
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getList()
    {
        $users = Sentry::findAllUsers();

        return View::make('admin.userlist', ['users' => $users]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        Sentry::logout();

        return Redirect::to('/');
    }
}
