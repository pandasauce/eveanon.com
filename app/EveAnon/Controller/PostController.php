<?php
namespace EveAnon\Controller;

use EveAnon\Exceptions\AccessForbiddenException;
use EveAnon\Model\Post;
use EveAnon\Model\User;
use EveAnon\Service\BlogService;
use Input;
use Redirect;
use Sentry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use View;

/**
 * Class PostController
 *
 * @package EveAnon\Controller
 */
class PostController extends BaseController
{
    /** @var User $actor */
    protected $actor;
    /** @var BlogService $blog_service */
    protected $blog_service;

    /**
     * @param BlogService $blog_service
     */
    public function __construct(BlogService $blog_service)
    {
        $this->actor = Sentry::getUser();
        $this->beforeFilter(
            'web.editors', [
                'only' =>
                    ['getEdit', 'postEdit', 'getDelete']
            ]
        );
        $this->blog_service = $blog_service;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $feed_data['feed'] = $this->blog_service->getFeed();
        $feed_data['title'] = 'Список постов';
        $feed_data['description'] = 'Список постов';

        return View::make('posts.feed', $feed_data);
    }

    /**
     * @param string $post_id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getFull($post_id = '')
    {
        if ($post_id == '') {
            return Redirect::action('EveAnon\Controller\PostController@getIndex');
        }
        $post = $this->blog_service->getPost($post_id);

        $post_data = [
            'post'        => $post,
            'title'       => $post['page_title'],
            'description' => $post['page_description']
        ];

        return View::make('posts.single', $post_data);
    }

    /**
     * @param $year
     * @param $month
     * @param $slug
     *
     * @return \Illuminate\View\View
     */
    public function getBySlug($year, $month, $slug)
    {
        $post = $this->blog_service->getPostBySlug($year, $month, $slug);

        $post_data = [
            'post'        => $post,
            'title'       => $post['page_title'],
            'description' => $post['page_description']
        ];

        return View::make('posts.single', $post_data);
    }

    /**
     * @param string $search_keywords
     *
     * @return \Illuminate\View\View
     */
    public function getSearch($search_keywords = null)
    {
        if (is_null($search_keywords)) {
            $search_keywords = Input::get('search_keywords');
        }

        $sanitised_keywords = htmlentities(trim(strip_tags($search_keywords)), ENT_QUOTES, 'UTF-8');

        $feed_data['feed'] = $this->blog_service->searchFeed($sanitised_keywords);
        $feed_data['title'] = 'Результаты поиска';
        $feed_data['description'] = 'Результаты поиска';

        return View::make('posts.feed', $feed_data);
    }

    /**
     * @param string $post_id
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($post_id = '')
    {
        if ($post_id == '') {
            // New post
            $editor_data['post'] = $this->blog_service->generateDummyPost();
            $editor_data['title'] = 'Создание новой записи';
            $editor_data['description'] = 'Создание новой записи';
        } else {
            // Existing post
            $editor_data['title'] = 'Редактирование записи';
            $editor_data['description'] = 'Редактирование записи';
            $editor_data['post'] = $this->blog_service->getPostForEdit($post_id, $this->actor);
            if (!$editor_data['post']) {
                // No post with such id
                throw new NotFoundHttpException;
            }
        }

        // Get paginated posts for sidebar feed
        $editor_data['feed'] = $this->blog_service->getFeed(false);

        return View::make('posts.editform', $editor_data);
    }

    /**
     * @param string $post_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit($post_id = '')
    {
        $post_data = Input::all();
        if ($post_id == '') {
            // Create a new post
            $post_id = $this->blog_service->writePost($post_data, $this->actor);
        } else {
            // Retrieve and update existing post
            $post_id = $this->blog_service->editPost($post_id, $post_data, $this->actor);
        }

        return Redirect::action('EveAnon\Controller\PostController@getEdit', [$post_id]);
    }

    /**
     * @param string $post_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($post_id = '')
    {
        if ($post_id != '') {
            $this->blog_service->deletePost($post_id, $this->actor);
        }

        return Redirect::action('EveAnon\Controller\PostController@getEdit');
    }
}
