<?php
namespace EveAnon\Controller;

use EveAnon\Service\SignatureService;
use Input;
use Redirect;
use Response;
use View;

/**
 * Class SignatureController
 *
 * @package EveAnon\Controller
 *          Index = List all signatures
 *          Show($systemName) = Show one system
 *          Create = Show one system and form for posting
 *          Store = Handler for Create form
 *          Edit = not used
 *          Update = not used, handled by Create
 *          Destroy = Purge signatures in one system
 */
class SignatureController extends BaseController
{
    protected $signatureService;

    public function __construct(SignatureService $signatureService)
    {
        $this->signatureService = $signatureService;
        $this->beforeFilter('eve-igb');
        $this->beforeFilter('eve-admin', array('only' => 'destroy'));
        $this->beforeFilter('eve-bans');
    }

    /**
     * List all signatures
     *
     * @return Response
     */
    public function index()
    {
        $signatures = $this->signatureService->getSignaturesInAllSystems();

        return View::make('signatures.index', array('signatures' => $signatures));
    }

    /**
     * Show one system and form for posting
     *
     * @return Response
     */
    public function create()
    {
        $system = $this->signatureService->getCurrentSystem();
        $signatures = $this->signatureService->getSignaturesInSystem($system);

        return View::make(
            'signatures.show',
            array('system' => $system, 'signatures' => $signatures)
        );
    }

    /**
     * Handler for Create form
     *
     * @return Response
     */
    public function store()
    {
        $system = $this->signatureService->getCurrentSystem();
        $character = $this->signatureService->getCurrentCharacter();
        $this->signatureService->handleScan(Input::get('probescan'), $system, $character);

        // redirect to create page
        return Redirect::to('signatures/create');
    }

    /**
     * Show one system
     *
     * @param  string $systemName
     *
     * @return Response
     */
    public function show($systemName)
    {
        $system = $this->signatureService->getSystem($systemName);
        $signatures = $this->signatureService->getSignaturesInSystem($system);

        return View::make(
            'signatures.show',
            array('system' => $system, 'signatures' => $signatures)
        );
    }

    /**
     * Purge signatures in one system
     *
     * @param  string $systemName
     *
     * @return Response
     */
    public function destroy($systemName)
    {
        $this->signatureService->purge($systemName);

        return Redirect::to('signatures/create');
    }
}
