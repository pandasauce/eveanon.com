<?php
namespace EveAnon\Command;

use Carbon\Carbon;
use EveAnon\Model\Signature;
use EveAnon\Service\SignatureService;
use Illuminate\Console\Command;

class SignaturesReset extends Command
{
    protected $signatureService;

    /**
     * Create a new command instance.
     *
     * @param SignatureService $signatureService
     *
     * @return \EveAnon\Command\SignaturesReset
     */
    public function __construct(SignatureService $signatureService)
    {
        parent::__construct();
        $this->signatureService = $signatureService;
    }

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'signatures:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kills outdated signatures';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $purged_amount = $this->signatureService->purgeOld();
        $this->info('Purged ' . $purged_amount . ' signatures at ' . Carbon::now());
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
