<?php
namespace EveApi;

use EveApi\Entity\Alliance;

class AllianceTest extends \TestCase
{
    public function testFromXml()
    {
        $alliance = Alliance::fromXml(99000163);

        $this->assertTrue(is_int($alliance->getAllianceID()));
        $this->assertTrue($alliance->getAllianceID() > 0);
        $this->assertTrue(is_string($alliance->getName()));
        $this->assertTrue($alliance->getName() != '');
        $this->assertTrue(count($alliance->getMemberCorporations()) > 0);
    }
}
