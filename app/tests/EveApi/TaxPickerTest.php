<?php
namespace EveApi;

use EveApi\Entity\Alliance;

class TaxPickerTest extends \TestCase
{
    protected $credentials;
    protected $connection;

    public function setUp()
    {
        parent::setUp();
        $this->credentials = new Credentials(
            '[REDACTED]',
            '[REDACTED]'
        );
        $this->connection = new Connection($this->credentials);
    }

    public function testPick()
    {
        $picker = new TaxPicker($this->connection);
        $alliances = $picker->pick(['434243723', '99005286'], 10, 200);
        /** @var Alliance $ccp_alliance */
        $ccp_alliance = $alliances->first();
        $corps = $ccp_alliance->getMemberCorporations();

        $this->assertInstanceOf('Illuminate\Support\Collection', $alliances);
        $this->assertTrue($corps->count() == 1);
    }
}
