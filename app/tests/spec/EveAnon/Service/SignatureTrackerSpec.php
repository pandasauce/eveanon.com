<?php

namespace spec\EveAnon\Service;

use EveAnon\Model\Alliance;
use EveAnon\Model\Character;
use EveAnon\Model\Corporation;
use EveAnon\Model\Signature;
use EveAnon\Model\System;
use EveAnon\Service\ScanParser;
use PhpSpec\Laravel\LaravelObjectBehavior;
use Prophecy\Argument;

class SignatureTrackerSpec extends LaravelObjectBehavior
{
    private $fake_probescan = "TII-489\tCosmic Anomaly\tCombat Site\tSansha Hidden Hub\t100.00%\t8.46 AU\r\nEJI-922\tCosmic Anomaly\tCombat Site\tSansha Forlorn Hub\t100.00%\t5.36 AU";

    function it_is_initializable()
    {
        $this->shouldHaveType('EveAnon\Service\SignatureTracker');
    }

    function it_detects_useful(Character $character)
    {
        $this->handleScan($this->fake_probescan, $this->getSystemInDb('MP5-KR'), $character)
             ->shouldReturn(1);
    }

    function it_detects_useless(Character $character)
    {
        $this->handleScan('', $this->getSystemInDb('MP5-KR'), $character)
             ->shouldReturn(0);
    }

    function let(ScanParser $parser)
    {
        // arrange
        $parser->probescan($this->fake_probescan)->willReturn($this->make_fake_signatures_array());
        $parser->probescan('')->willThrow('\EveAnon\Exceptions\NotProbescanException');
        $this->beConstructedWith($parser);
    }

    private function make_fake_signatures_array()
    {
        // arrange
        $result[0] = new Signature;
        $result[0]->id = 'TII-489';
        $result[0]->scan_group = 'Cosmic Anomaly';
        $result[0]->group = 'Combat Site';
        $result[0]->type = 'Sansha Hidden Hub';
        $result[0]->signal = '100.00%';
        $result[0]->distance = '8.46 AU';

        $result[1] = new Signature;
        $result[1]->id = 'EJI-922';
        $result[1]->scan_group = 'Cosmic Anomaly';
        $result[1]->group = 'Combat Site';
        $result[1]->type = 'Sansha Forlorn Hub';
        $result[1]->signal = '100.00%';
        $result[1]->distance = '5.36 AU';

        return $result;
    }

    protected function getSystemInDb($systemName)
    {
        $system = new System;
        $system->id = $systemName;

        return $system;
    }
}
