<?php

namespace spec\EveAnon\Service;

use Carbon\Carbon;
use EveAnon\Model\Signature;
use EveAnon\Service\Igb;
use EveAnon\Service\SignatureTracker;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use PhpSpec\Laravel\LaravelObjectBehavior;
use Prophecy\Argument;

class SignatureServiceSpec extends LaravelObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('EveAnon\Service\SignatureService');
    }

    private function seed_downtime_signatures()
    {
        Signature::truncate();
        // OK: Today > 11
        $signature = new Signature;
        $signature->unguard();
        $signature->create(
            [
                'id'         => 'DVU-725',
                'scan_group' => 'Cosmic Anomaly',
                'group'      => 'Combat Site',
                'type'       => 'Drone Horde',
                'signal'     => '100.00%',
                'distance'   => '4.79 AU',
                'system_id'  => 'MP5-KR',
                'created_at' => Carbon::today()->hour(13)
            ]
        );
        $signature->reguard();
        // SOFT: Yesterday > 11
        $signature = new Signature;
        $signature->unguard();
        $signature->create(
            [
                'id'         => 'UJF-902',
                'scan_group' => 'Cosmic Anomaly',
                'group'      => 'Combat Site',
                'type'       => 'Sansha Port',
                'signal'     => '100.00%',
                'distance'   => '4.42 AU',
                'system_id'  => 'MP5-KR',
                'created_at' => Carbon::yesterday()->hour(13)
            ]
        );
        $signature->reguard();
        // HARD: Pre-Yesterday > 11
        $signature = new Signature;
        $signature->unguard();
        $signature->create(
            [
                'id'         => 'OTG-632',
                'scan_group' => 'Cosmic Anomaly',
                'group'      => 'Combat Site',
                'type'       => 'Sansha Forsaken Hub',
                'signal'     => '100.00%',
                'distance'   => '5.33 AU',
                'system_id'  => 'MP5-KR',
                'created_at' => Carbon::yesterday()->subDay()->hour(13)
            ]
        );
        $signature->reguard();
    }

    function it_invalidates_by_downtime()
    {
        $this->seed_downtime_signatures();
        // Count test signatures
        $this->getTotalActiveCount()->shouldReturn(3);
        // Delete expired signatures
        $this->purgeDowntime()->shouldReturn(2);
        // Count test signatures
        $this->getTotalActiveCount()->shouldReturn(1);
        // Ensure soft deletion worked correctly
        Signature::findOrFail('DVU-725');
        // Ensure force deletion worked correctly
        try {
            Signature::findOrFail('OTG-632');
            throw new \Exception('This signature should have been force deleted');
        } catch (ModelNotFoundException $e) {
        }
    }

    private function seed_now_signatures()
    {
        Signature::truncate();
        // OK: Today > 11
        $signature = new Signature;
        $signature->unguard();
        $signature->create(
            [
                'id'         => 'DVU-725',
                'scan_group' => 'Cosmic Anomaly',
                'group'      => 'Combat Site',
                'type'       => 'Drone Horde',
                'signal'     => '100.00%',
                'distance'   => '4.79 AU',
                'system_id'  => 'MP5-KR',
                'created_at' => Carbon::now()
            ]
        );
        $signature->reguard();
        // SOFT: Yesterday > 11
        $signature = new Signature;
        $signature->unguard();
        $signature->create(
            [
                'id'         => 'UJF-902',
                'scan_group' => 'Cosmic Anomaly',
                'group'      => 'Combat Site',
                'type'       => 'Sansha Port',
                'signal'     => '100.00%',
                'distance'   => '4.42 AU',
                'system_id'  => 'MP5-KR',
                'created_at' => Carbon::now()->subDay()
            ]
        );
        $signature->reguard();
        // HARD: Pre-Yesterday > 11
        $signature = new Signature;
        $signature->unguard();
        $signature->create(
            [
                'id'         => 'OTG-632',
                'scan_group' => 'Cosmic Anomaly',
                'group'      => 'Combat Site',
                'type'       => 'Sansha Forsaken Hub',
                'signal'     => '100.00%',
                'distance'   => '5.33 AU',
                'system_id'  => 'MP5-KR',
                'created_at' => Carbon::now()->subDays(2)->subHour()
            ]
        );
        $signature->reguard();
    }

    function it_deletes_definitely_expired()
    {
        $this->seed_now_signatures();
        $this->getTotalActiveCount()->shouldReturn(3);
        $this->purgeOld()->shouldReturn(1);
        $this->getTotalActiveCount()->shouldReturn(2);
    }

    function let(Igb $igb, SignatureTracker $signatureTracker)
    {
        // arrange
        $igb->isAdminMode()->willReturn(true);
        $this->beConstructedWith($igb, $signatureTracker);
    }
}
