<?php

namespace spec\EveAnon\Service;

use EveAnon\Model\Signature;
use PhpSpec\Laravel\LaravelObjectBehavior;
use Prophecy\Argument;

class ScanParserSpec extends LaravelObjectBehavior
{
    private $fake_probescan = "TII-489\tCosmic Anomaly\tCombat Site\tSansha Hidden Hub\t100.00%\t8.46 AU\r\nEJI-922\tCosmic Anomaly\tCombat Site\tSansha Forlorn Hub\t100.00%\t5.36 AU";

    function it_is_initializable()
    {
        $this->shouldHaveType('EveAnon\Service\ScanParser');
    }

    function it_parses_probescan_line_into_signature()
    {
        $this->signature(explode("\r\n", $this->fake_probescan)[0])
            ->shouldBeLike($this->make_fake_signatures_array()[0]);
    }

    function it_parses_probescan_into_array_of_signatures()
    {
        $this->probescan($this->fake_probescan)
            ->shouldBeLike($this->make_fake_signatures_array());
    }

    private function make_fake_signatures_array()
    {
        $result[0] = new Signature;
        $result[0]->id = 'TII-489';
        $result[0]->scan_group = 'Cosmic Anomaly';
        $result[0]->group = 'Combat Site';
        $result[0]->type = 'Sansha Hidden Hub';
        $result[0]->signal = '100.00%';
        $result[0]->distance = '8.46 AU';

        $result[1] = new Signature;
        $result[1]->id = 'EJI-922';
        $result[1]->scan_group = 'Cosmic Anomaly';
        $result[1]->group = 'Combat Site';
        $result[1]->type = 'Sansha Forlorn Hub';
        $result[1]->signal = '100.00%';
        $result[1]->distance = '5.36 AU';

        return $result;
    }
}
