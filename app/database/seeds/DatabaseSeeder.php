<?php

use EveAnon\Model\Post;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->seedUsers();
        if (App::environment() == 'production') {
            $this->seedLegacyPosts();
        } else {
            $this->seedFakerPosts();
        }
    }

    protected function getLegacyPostsDirectory()
    {
        return storage_path() . "/pages/";
    }

    protected function getLegacyPostPath($legacy_post_name)
    {
        return file_get_contents($this->getLegacyPostsDirectory() . $legacy_post_name);
    }

    protected function seedUsers()
    {
        // Groups
        $adminGroup = Sentry::createGroup(
            array(
                'name'        => 'Superusers',
                'permissions' => array(
                    'web.edit'  => 1,
                    'web.admin' => 1,
                ),
            )
        );
        $editorGroup = Sentry::createGroup(
            [
                'name'        => 'Editors',
                'permissions' => [
                    'web.edit'  => 1,
                    'web.admin' => 0,
                ]
            ]
        );
        // Users
        $user = Sentry::createUser(
            array(
                'email'      => 'batya@eveanon.com',
                'password'   => '[REDACTED]',
                'activated'  => true,
                'first_name' => 'Батя',
            )
        );
        $user_agafon = Sentry::createUser(
            [
                'email'      => 'agafon@eveanon.com',
                'password'   => '[REDACTED]',
                'activated'  => true,
                'first_name' => 'Агафья',
            ]
        );
        $user_tomka = Sentry::createUser(
            [
                'email'      => 'tomka@eveanon.com',
                'password'   => '[REDACTED]',
                'activated'  => true,
                'first_name' => 'Томка',
            ]
        );
        $user_politics = Sentry::createUser(
            [
                'email'      => 'politics@eveanon.com',
                'password'   => '[REDACTED]',
                'activated'  => true,
                'first_name' => 'Политик',
            ]
        );
        $user_ful = Sentry::createUser(
            [
                'email'      => 'ful@eveanon.com',
                'password'   => '[REDACTED]',
                'activated'  => true,
                'first_name' => 'Fulminate',
            ]
        );
        $user->addGroup($adminGroup);
        $user_agafon->addGroup($editorGroup);
        $user_tomka->addGroup($editorGroup);
        $user_politics->addGroup($editorGroup);
        $user_ful->addGroup($editorGroup);
    }

    protected function seedLegacyPosts()
    {
        // Posts
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "EVE ISK Anon Edition",
                "longTitle"       => "EVE ISK Anon Edition",
                "postDescription" => "Способы заработка игровой валюты ISK в EVE Online, гайд двача",
                "slug"            => "eve-isk-anon-edition",
                "rawHtml"         => $this->getLegacyPostPath("isk.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2013-09-05 17:45:00',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Рассказ о рабстве в нулях",
                "longTitle"       => "Как живут рабы в нулевых альянсах EVE Online",
                "postDescription" => "Как живут рабы в нулевых альянсах EVE Online, что такое КТА и как выглядит ПВП",
                "slug"            => "rasskaz-o-rabstve-v-nulyah",
                "rawHtml"         => $this->getLegacyPostPath("ctaslavery.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2013-10-26 00:38:00',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "NoobMeat - откровения чулочника",
                "longTitle"       => "NoobMeat - откровения чулочника",
                "postDescription" => "История крупнейшей корпорации анонов в EVE Online.",
                "slug"            => "noobmeat-otkroveniya-chulochnika",
                "rawHtml"         => $this->getLegacyPostPath("noobmeat.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2013-11-22 18:53:00',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "EVE Online - куда податься",
                "longTitle"       => "EVE Online - куда податься",
                "postDescription" => "Детальный анализ текущей обстановки в рабовладельческом бизнесе русскоязычного сегмента EVE Online.",
                "slug"            => "kuda-podatsya",
                "rawHtml"         => $this->getLegacyPostPath("alliances.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2013-11-24 13:53:00',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Чемоданное BINGO",
                "longTitle"       => "Чемоданное BINGO",
                "postDescription" => "Увлекательная игра, в которую можно играть всем форумом!",
                "slug"            => "chemodannoye-bingo",
                "rawHtml"         => "<p>Увлекательная игра, в которую можно играть всем форумом!</p>\n<p><img src=\"/img/flags/gb.gif\"> <a href=\"/img/dcbingoEN.png\">English version</a>\n<br /><img src=\"/img/flags/ru.gif\"> <a href=\"/img/dcbingoRU.png\">Русская версия</a></p>",
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2013-12-09 18:39:56',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Как пользоваться ESS в EVE Online",
                "longTitle"       => "Увеличиваем свой <del>членис</del> доход",
                "postDescription" => "Как увеличить свой заработок правильно и безопасно используя Encounter Surveillance System",
                "slug"            => "kak-polzovatsya-ess",
                "rawHtml"         => $this->getLegacyPostPath("enlargepenis.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-02-06 21:17:13',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "EVE Online - как проходить релики",
                "longTitle"       => "EVE Online - как проходить релики",
                "postDescription" => "Ебашим релики как боги - 1ккк в день на ковре в нулях без риска",
                "slug"            => "kak-prohodit-reliki",
                "rawHtml"         => $this->getLegacyPostPath("explore.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-03-10 16:22:46',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Вся правда о продаже -DD-",
                "longTitle"       => "Вся правда о дизбанде Darkness of Despair",
                "postDescription" => "Сказ о том, как и зачем на самом деле создавался главный русскоязычный альянс последнего года и что не рассказали грунтам о дизбанде",
                "slug"            => "pravda-o-dd",
                "rawHtml"         => $this->getLegacyPostPath("darknessofrmt.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-03-15 19:57:17',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "EVE Online - первый пермабан за харассмент",
                "longTitle"       => "Как не нарушить EULA и получить пермабан в EVE Online",
                "postDescription" => "28 марта 2014 был выдан первый пермабан за харассмент в EVE Online. Erotica 1 - лучший тролль во всей истории евы.",
                "slug"            => "pervi-permaban-za-harassment",
                "rawHtml"         => $this->getLegacyPostPath("erotica1.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-03-29 13:46:55',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Гайд по PVP для новичков в EVE Online",
                "longTitle"       => "Гайд по PVP для новичков в EVE Online",
                "postDescription" => "Гайд по пвп pvp для новичков начинающих мало сп в eve online еве",
                "slug"            => "guide-po-pvp",
                "rawHtml"         => $this->getLegacyPostPath("pvp.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-03-31 21:00:22',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Как распускают альянс честные люди",
                "longTitle"       => "Как распускают альянс честные люди",
                "postDescription" => "Дизбанд WALLTREIPERS ALLIANCE. Как распускают альянс честные люди, не предающие своих друзей за наличность.",
                "slug"            => "kak-raspuskayut-alyans",
                "rawHtml"         => $this->getLegacyPostPath("news/kak-raspuskayut-alyans.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-04-13 00:25:13',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Fanfest 2014 - трейлер",
                "longTitle"       => "Fanfest 2014 - трейлер",
                "postDescription" => "Трейлер с завершающей презентации EVE Fanfest 2014",
                "slug"            => "fanfest-2014-trailer",
                "rawHtml"         => $this->getLegacyPostPath("news/fanfest-2014-trailer.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-05-04 10:37:25',
            )
        );
        Post::create(
            array(
                "user_id"         => 1,
                "shortTitle"      => "Пятница на EVE-RU",
                "longTitle"       => "Пятница на EVE-RU",
                "postDescription" => "Плагин добавляет ватермарки в записи TeamSpeak и передаёт эти данные на сервер Darkspawn. Особо отличившийся WarStalkeR затроллил всех некомпетентных в вопросе читателей, придумав именно ту историю, которую все хотели услышать и сорвал часть баунти. Пол форума до сих пор верит, что Darkspawn раздавали своим мемберам кейлоггер. 30 страниц поливания друг друга низкопробной грязью.",
                "slug"            => "pyatnica-na-everu",
                "rawHtml"         => $this->getLegacyPostPath("news/pyatnica-na-everu.html"),
                "boolPublic"      => true,
                "boolComments"    => true,
                "created_at"      => '2014-08-09 24:47:37',
            )
        );
    }

    /**
     * Generate posts with random gibberish.
     *
     * @param int $amount
     */
    protected function seedFakerPosts($amount = 17)
    {
        $faker = Faker\Factory::create('ru_RU');
        for ($i = 0; $i < $amount; $i++) {
            Post::create(
                array(
                    'user_id'         => rand(1,3),
                    'shortTitle'      => $faker->text($faker->numberBetween(5, 25)),
                    'longTitle'       => $faker->text($faker->numberBetween(5, 50)),
                    'postDescription' => $faker->text($faker->numberBetween(5, 140)),
                    'slug'            => $faker->slug,
                    'rawHtml'         => nl2br($faker->text($faker->numberBetween(500, 800))) . '<!-- more -->' . nl2br($faker->text($faker->numberBetween(500, 800))),
                    'boolPublic'      => true,
                    'boolComments'    => true,
                    'created_at'      => $faker->dateTimeBetween('-4 years')->format('Y-m-d H:i:s'),
                )
            );
        }
    }
}
