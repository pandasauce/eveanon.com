<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorporationSignatureTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'corporation_signature', function (Blueprint $table) {
                $table->integer('corporation_id');
                $table->string('signature_id');
                $table->timestamps();
                $table->primary(['corporation_id', 'signature_id']);

                $table->foreign('corporation_id')->references('id')->on('corporations');
                $table->foreign('signature_id')->references('id')->on('signatures')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporation_signature');
    }

}
