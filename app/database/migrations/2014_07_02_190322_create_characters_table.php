<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'characters', function (Blueprint $table) {
                $table->integer('id')->primary();
                $table->string('name');
                $table->integer('points')->default('0');
                $table->integer('corporation_id');
                $table->timestamps();

                $table->foreign('corporation_id')->references('id')->on('corporations');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }

}
