<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'systems', function (Blueprint $table) {
                $table->string('id');
                $table->string('constellation_id')->nullable();
                $table->primary('id');
                $table->foreign('constellation_id')->references('id')->on('constellations');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }

}
