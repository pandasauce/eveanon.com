<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddEntityTickers extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'alliances',
            function (Blueprint $table) {
                $table->string('ticker', 5);
            }
        );
        Schema::table(
            'corporations',
            function (Blueprint $table) {
                $table->string('ticker', 5);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('alliances', 'ticker')) {
            Schema::table(
                'alliances',
                function (Blueprint $table) {
                    $table->dropColumn('ticker');
                }
            );
        }
        if (Schema::hasColumn('corporations', 'ticker')) {
            Schema::table(
                'corporations',
                function (Blueprint $table) {
                    $table->dropColumn('ticker');
                }
            );
        }
    }

}
