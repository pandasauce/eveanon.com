<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorporationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'corporations', function (Blueprint $table) {
                $table->integer('id')->primary();
                $table->string('name');
                $table->integer('alliance_id')->nullable();
                $table->timestamps();

                $table->foreign('alliance_id')->references('id')->on('alliances');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporations');
    }

}
