<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSignaturesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'signatures', function (Blueprint $table) {
                $table->string('id');
                $table->string('scan_group');
                $table->string('group');
                $table->string('type');
                $table->double('signal');
                $table->string('distance');
                $table->string('system_id');
                $table->timestamps();
                $table->softDeletes();
                // Keys
                $table->primary('id');
                $table->foreign('system_id')->references('id')->on('systems');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signatures');
    }

}