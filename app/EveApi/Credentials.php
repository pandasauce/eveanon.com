<?php
namespace EveApi;

/**
 * Class Credentials
 *
 * @package  EveApi
 * @author   Georgi Boiko <georgi@pandasauce.org>
 * @date     2015.3.29
 */
class Credentials
{
    /** @var string keyID */
    protected $keyID;
    /** @var string vCode */
    protected $vCode;

    /**
     * @param string $keyID
     * @param string $vCode
     */
    public function __construct($keyID, $vCode)
    {
        $this->keyID = $keyID;
        $this->vCode = $vCode;
    }

    /**
     * @return string
     */
    public function getKeyID()
    {
        return $this->keyID;
    }

    /**
     * @return string
     */
    public function getVCode()
    {
        return $this->vCode;
    }
}
