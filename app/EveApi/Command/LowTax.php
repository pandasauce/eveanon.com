<?php
namespace EveApi\Command;

use EveApi\TaxPicker;
use Illuminate\Console\Command;
use View;

class LowTax extends Command
{
    protected $taxPicker;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'api:lowtax';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses the lowtax citizen corps';

    /**
     * Create a new command instance.
     *
     * @param TaxPicker $taxPicker
     */
    public function __construct(TaxPicker $taxPicker)
    {
        parent::__construct();
        $this->taxPicker = $taxPicker;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Executing lowtax picker');
        $live_alliances = ['99003500', '99000163', '99003006', '99001783', '99005443', '99006239'];
        $test_alliances = ['434243723', '99005286'];
        $alliance_ids = (\App::environment() == 'production' ? $live_alliances : $test_alliances);
        $this->info('Loading alliances: ' . implode(', ', $alliance_ids));
        $alliances = $this->taxPicker->pick($alliance_ids, 11, 10);
        $this->info('Finished loading alliances');
        $view = View::make('api.lowtax', ['alliances' => $alliances]);
        $viewpath = storage_path('pages/api/corpsTable.html');
        file_put_contents($viewpath, $view->render());
        $this->info('Finished executing lowtax picker');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
