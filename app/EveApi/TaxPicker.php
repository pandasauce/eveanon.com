<?php
namespace EveApi;

use EveApi\Entity\Alliance;
use Illuminate\Support\Collection;

class TaxPicker
{
    /** @var Connection $connection */
    protected $connection;

    /**
     * @param Connection $connection
     */
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $alliance_ids
     * @param float $tax_threshold
     * @param int   $members_threshold
     *
     * @return Entity\Alliance[]|Collection
     */
    public function pick($alliance_ids, $tax_threshold, $members_threshold)
    {
        /** @var Alliance[]|Collection $alliances */
        $alliances = new Collection([]);
        foreach ($alliance_ids as $id) {
            $alliance = Alliance::fromXml($id);
            $alliance->loadApiCorpsData($this->connection);
            $alliances->push($alliance);
        }

        $alliances->each(
            function (Alliance $alliance) use ($tax_threshold, $members_threshold) {
                $corps = $alliance->getMemberCorporations();
                $filtered_corps = new Collection([]);
                foreach ($corps as $corp) {
                    $tax_valid = $corp->getTaxRate() <= $tax_threshold;
                    $members_valid = $corp->getMemberCount() >= $members_threshold;
                    if ($tax_valid && $members_valid) {
                        $filtered_corps->push($corp);
                    }
                }
                $alliance->setMemberCorporations($filtered_corps);
            }
        );

        return $alliances;
    }
}
