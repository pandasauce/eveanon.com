<?php
namespace EveApi\Entity;

use Carbon\Carbon;
use EveApi\Connection;

class Corporation
{
    /** @var int $corporationID */
    protected $corporationID;
    /** @var string $corporationName */
    protected $corporationName;
    /** @var Carbon $startDate */
    protected $startDate;
    /** @var float $taxRate */
    protected $taxRate;
    /** @var int $memberCount */
    protected $memberCount;
    /** @var string $rawApiData */
    protected $rawApiData;

    /**
     * @param int    $corporationID
     * @param string $startDate
     */
    public function __construct($corporationID, $startDate)
    {
        $this->corporationID = $corporationID;
        $this->startDate = Carbon::parse($startDate);
    }

    /**
     * @return int
     */
    public function getCorporationID()
    {
        return $this->corporationID;
    }

    /**
     * @return Carbon
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return float
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * @return int
     */
    public function getMemberCount()
    {
        return $this->memberCount;
    }

    /**
     * @param Connection $connection
     */
    public function loadApiData($connection)
    {
        if (is_null($this->rawApiData)) {
            $this->rawApiData = $connection->getCorporationInfo($this);
        }
        $this->taxRate = $this->rawApiData->taxRate;
        $this->memberCount = $this->rawApiData->memberCount;
        $this->corporationName = $this->rawApiData->corporationName;
    }

    /**
     * @return string
     */
    public function getCorporationName()
    {
        return $this->corporationName;
    }
}
