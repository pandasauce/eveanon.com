<?php
namespace EveApi\Entity;

use Carbon\Carbon;
use EveApi\Connection;
use Illuminate\Support\Collection;
use Pheal\Pheal;

class Alliance
{
    /** @var string $xmlFile */
    protected static $xmlFile = 'pages/api/AllianceList.xml';
    /** @var string $xmlRaw */
    protected static $xmlRaw;
    /** @var int $allianceID */
    protected $allianceID;
    /** @var string $name */
    protected $name;
    /** @var string $shortName */
    protected $shortName;
    /** @var int $executorCorpID */
    protected $executorCorpID;
    /** @var int $memberCount */
    protected $memberCount;
    /** @var Carbon $startDate */
    protected $startDate;
    /** @var Corporation[]|Collection $memberCorporations */
    protected $memberCorporations;

    /**
     * @param Corporation[]|Collection $memberCorporations
     */
    public function setMemberCorporations($memberCorporations)
    {
        $this->memberCorporations = $memberCorporations;
    }

    /**
     * @param int    $allianceID
     * @param string $name
     * @param string $shortName
     * @param int    $executorCorpID
     * @param int    $memberCount
     * @param string $startDate
     */
    public function __construct($allianceID, $name, $shortName, $executorCorpID, $memberCount, $startDate)
    {
        $this->allianceID = $allianceID;
        $this->name = $name;
        $this->shortName = $shortName;
        $this->executorCorpID = $executorCorpID;
        $this->memberCount = $memberCount;
        $this->startDate = Carbon::parse($startDate);
        $this->memberCorporations = new Collection([]);
    }

    /**
     * @param int $id
     *
     * @return static
     */
    public static function fromXml($id)
    {
        if (is_null(static::$xmlRaw)) {
            $xml_path = storage_path(static::$xmlFile);
            $exists = file_exists($xml_path);
            if ($exists) {
                $last_modded = Carbon::createFromTimestamp(filemtime($xml_path));
            }
            if (!$exists || $last_modded->lt(Carbon::now()->subDay())) {
                \Log::info('AllianceList.xml older than 1 day, grabbing one from CCP');
                exec('wget https://api.eveonline.com/eve/AllianceList.xml.aspx -O ' . $xml_path);
                static::$xmlRaw = file_get_contents($xml_path);
                \Log::info('Finished grabbing AllianceList.xml from CCP');
            } else {
                static::$xmlRaw = file_get_contents($xml_path);
            }
        }

        $xml = new \SimpleXMLElement(static::$xmlRaw);
        $xml_alliances = $xml->xpath("./result/rowset/row[@allianceID='" . $id . "']");
        $xml_alliance = $xml_alliances[0];

        $instance = new static(
            (int)$xml_alliance['allianceID'],
            (string)$xml_alliance['name'],
            (string)$xml_alliance['shortName'],
            (int)$xml_alliance['executorCorpID'],
            (int)$xml_alliance['memberCount'],
            (string)$xml_alliance['startDate']
        );

        // corps
        $xml_corps = $xml->xpath("./result/rowset/row[@allianceID='" . $id . "']/rowset[@name='memberCorporations']/*");
        foreach ($xml_corps as $raw_corp) {
            $corp = new Corporation(
                (int)$raw_corp['corporationID'],
                (string)$raw_corp['startDate']
            );
            $instance->addCorporation($corp);
        }

        return $instance;
    }

    /**
     * @param Corporation $corporation
     */
    public function addCorporation($corporation)
    {
        $this->memberCorporations->push($corporation);
    }

    /**
     * @return int
     */
    public function getAllianceID()
    {
        return $this->allianceID;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @return int
     */
    public function getExecutorCorpID()
    {
        return $this->executorCorpID;
    }

    /**
     * @return int
     */
    public function getMemberCount()
    {
        return $this->memberCount;
    }

    /**
     * @return Carbon
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return Corporation[]|Collection
     */
    public function getMemberCorporations()
    {
        return $this->memberCorporations;
    }

    /**
     * @param Connection $connection
     */
    public function loadApiCorpsData($connection)
    {
        foreach ($this->memberCorporations as $corp) {
            $corp->loadApiData($connection);
        }
    }
}
