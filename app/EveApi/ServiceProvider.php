<?php
namespace EveApi;

use Illuminate\Support\ServiceProvider as BaseProvider;

class ServiceProvider extends BaseProvider
{
    public function boot()
    {
        $this->commands('EveApi\Command\LowTax');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'EveApi\TaxPicker',
            function () {
                $credentials = new Credentials(
                    '[REDACTED]',
                    '[REDACTED]'
                );
                $connection = new Connection($credentials);

                $picker = new TaxPicker($connection);

                return $picker;
            }
        );
    }
}
