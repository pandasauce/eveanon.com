<?php
namespace EveApi;

use EveApi\Entity\Corporation;
use Pheal\Access\StaticCheck;
use Pheal\Cache\FileStorage;
use Pheal\Core\Config;
use Pheal\Pheal;

class Connection
{
    /** @var Credentials $credentials */
    protected $credentials;
    /** @var Pheal $pheal */
    protected $pheal;

    /**
     * @param Credentials $credentials
     */
    public function __construct($credentials)
    {
        $this->credentials = $credentials;
        $this->connect();
    }

    /**
     * @return void
     */
    protected function connect()
    {
        Config::getInstance()->cache = new FileStorage(storage_path('pages/api/cache/'));
        Config::getInstance()->access = new StaticCheck();
        $this->pheal = new Pheal($this->credentials->getKeyID(), $this->credentials->getVCode());
    }

    /**
     * @param Corporation $corporation
     *
     * @return object
     */
    public function getCorporationInfo($corporation)
    {
        $this->pheal->scope = 'corp';
        $response = $this->pheal->CorporationSheet(
            [
                'corporationId' => $corporation->getCorporationID()
            ]
        );

        return $response;
    }
}
