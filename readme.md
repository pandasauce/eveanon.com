An EVE Online community I used to run as a student. Now published under [CC-BY-NC-SA license](/LICENSE) for the primary purpose of referring people who want to see how I write/used to write my code to this repository.

It was a nice side project for developing my OOP skills, experimenting with frameworks, patterns, API integrations and practicing TDD.
Written with Laravel 4.2, PHP 5.5, Python 2.6 and occasional JS.

I had to redact some values and nuke Git history prior to publishing to avoid posting sensitive data such as API keys. Search and replace instances of `[REDACTED]` if you are going to run this code.